package com.babe.auth.common.constant;

/**
 * Description
 * Datetime: 2020/3/6 15:15
 * Author:jason
 */
public class BabeConstant {

    // 前端页面路径前缀
    public static final String VIEW_PREFIX = "babe/views/";

}
