package com.babe.auth.common.mq.sender;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Description 生产者，则产生消息的客户端，将消息推到exchange中
 * Datetime: 2020/3/1 17:37
 * Author:jason
 */
@Slf4j
@Component
public class Sender {

    @Autowired
    RabbitTemplate rabbitTemplate;
    public void send() {
        String message =  "message:" + "中国加油，武汉加油，世界加油，我们能赢";
        log.info("Sender者发送的信息为  " + message);
        rabbitTemplate.convertAndSend("immediate_exchange_test1", "immediate_routing_key_test1", message);
    }
}
