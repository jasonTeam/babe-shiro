package com.babe.auth.common.mq.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;


/**
 * rabbitmq的使用过程
 * rabbitmq的使用过程大概如下：
 *
 * 1、客户端连接到消息队列服务器，打开一个channel。
 * 2、客户端声明一个exchange，并设置相关属性,相关属性包括exchange的名字，exchange是否可以被持久化(持久化到磁盘)，exchange是否自动删除
 * 3、客户端声明一个queue，并设置相关属性，相关属性包括queue的名字，queue是否可以被持久化(持久化到磁盘)，queue是否自动删除
 * 4、声明exchange和queue之间的binding关系，用唯一的key来进行绑定
 * 5、客户端投递消息到exchange。
 * 6、不同类型的exchange根据路由规则将消息分发到不同的queue中
 * 7、客户端监听到queue中的内容，将消息取出消费，发送ack到rabbitmq中，删除该条消息
 * 原文链接：https://blog.csdn.net/zhangyuxuan2/article/details/82986702
 *
 * Description 初始化rabbitmq的exchange,queue以及他们之间的绑定关系
 * Datetime: 2020/3/1 17:34
 * Author:jason
 */
@Configuration
public class RabbitmqConfig {

    // 创建一个立即消费队列
    @Bean
    public Queue immediateQueue() {
        // 第一个参数是创建的queue的名字，第二个参数是是否支持持久化
        return new Queue("immediate_queue_test1", true);
    }

    @Bean
    public DirectExchange immediateExchange() {
        // 一共有三种构造方法，可以只传exchange的名字， 第二种，可以传exchange名字，是否支持持久化，是否可以自动删除，
        //第三种在第二种参数上可以增加Map，Map中可以存放自定义exchange中的参数
        return new DirectExchange("immediate_exchange_test1", true, false);
    }

    @Bean
    //把立即消费的队列和立即消费的exchange绑定在一起
    public Binding immediateBinding() {
        return BindingBuilder.bind(immediateQueue()).to(immediateExchange()).with("immediate_routing_key_test1");
    }

}
