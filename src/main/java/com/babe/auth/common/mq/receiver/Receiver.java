package com.babe.auth.common.mq.receiver;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * Description 消费者，则消费消息的客户端
 * Datetime: 2020/3/1 17:40
 * Author:jason
 */
@Slf4j
@Component
public class Receiver {

    @RabbitHandler
    @RabbitListener(queues = "immediate_queue_test1")
    public void immediateProcess(String message) {
        log.info("接收者接收到的信息为receiver=[{}]:",message);
    }

}
