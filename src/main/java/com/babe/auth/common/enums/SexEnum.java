package com.babe.auth.common.enums;

/**
 * Description 性别枚举
 * Datetime: 2020/2/15 22:20
 * Author:jason
 */
public enum SexEnum {

    MAN("0", "男"),
    WOMAN("1", "女"),
    UNKNOWN("2", "保密"),;
    private String value;
    private String desc;

    //构造器，初始化属性
    private SexEnum(String str, String describe) {
        this.value = str;
        this.desc = describe;
    }

    public static SexEnum getEnumByValue(String val) {
        if (null == val) {
            return null;
        }
        for (SexEnum enumStr : SexEnum.values()) {
            if (enumStr.getValue().equals(val)) {
                return enumStr;
            }
        }
        return null;
    }

    public static SexEnum getEnumByDesc(String desc) {
        if (null == desc) {
            return null;
        }
        for (SexEnum enumStr : SexEnum.values()) {
            if (enumStr.getDesc().equals(desc)) {
                return enumStr;
            }
        }
        return null;
    }

    public String getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }

}
