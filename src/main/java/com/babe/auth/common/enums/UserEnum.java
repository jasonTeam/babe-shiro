package com.babe.auth.common.enums;

/**
 * Description 用户相关的枚举类
 * Datetime: 2020/2/15 22:20
 * Author:jason
 */
public enum UserEnum {

    USER_STATUS_NORMAL ("1","有效"),
    USER_STATUS_LOCK ("0","锁定"),
    DEF_AVATAR("default.jpg","用户默认头像"),
    DEF_THEME("black","黑色主题"),
    IS_TAB("1","开启tab"),
    DEF_PWD("888888","默认密码")
    ;

    private String value;
    private String desc;

    UserEnum(String value,String desc){
        this.value = value;
        this.desc = desc;
    }

    public String value(){
        return this.value;
    }

    public String desc(){
        return this.desc;
    }


}
