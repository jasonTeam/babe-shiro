package com.babe.auth.common.exception;

/**
 * Description
 * Datetime: 2020/2/14 13:33
 * Author:jason
 */
public class BabeException extends RuntimeException{

    public BabeException(String message){
        super(message);
    }

}
