package com.babe.auth.common.utils.encry;

import java.security.Security;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// 来源于中钢demo add by jason 2020/03/14
public class AesUtil {

    private static final Logger logger = LoggerFactory.getLogger(AesUtil.class);

    public static final String CHARSET = "UTF-8";

    public static final String ALGORITHM = "AES/ECB/PKCS5Padding";

    static {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    /**
     * 网联敏感字段AES加密
     *
     * @param str 敏感字段原文
     * @param key AES密钥.getByte()
     * @return
     */
    public static byte[] Aes256Encode(String str, byte[] key) {
        byte[] result = null;
        try {
            Cipher cipher = Cipher.getInstance(ALGORITHM, "BC");
            SecretKeySpec keySpec = new SecretKeySpec(key, "AES"); // 生成加密解密需要的Key
            cipher.init(Cipher.ENCRYPT_MODE, keySpec);
            result = cipher.doFinal(str.getBytes(CHARSET));
        } catch (Exception e) {
            logger.error("Aes256Encode error:", e);
        }
        return result;
    }

    public static String Aes256EncodeWithBase64(String str, String key) {
        try {
            Cipher cipher = Cipher.getInstance(ALGORITHM, "BC");
            SecretKeySpec keySpec = new SecretKeySpec(key.getBytes(CHARSET), "AES"); // 生成加密解密需要的Key
            cipher.init(Cipher.ENCRYPT_MODE, keySpec);
            byte[] result = cipher.doFinal(str.getBytes(CHARSET));
            return Base64.encodeBase64String(result);
        } catch (Exception e) {
            logger.error("Aes256Encode error:", e);
        }
        return null;
    }

    /**
     * 网联敏感字段密文解密
     *
     * @param bytes 要被解密的密文.getByte
     * @param key   AES密钥.getByte
     * @return 解密后的字符串
     */
    public static String Aes256Decode(byte[] bytes, byte[] key) {
        String result = null;
        try {
            Cipher cipher = Cipher.getInstance(ALGORITHM, "BC");
            SecretKeySpec keySpec = new SecretKeySpec(key, "AES"); // 生成加密解密需要的Key
            cipher.init(Cipher.DECRYPT_MODE, keySpec);
            byte[] decoded = cipher.doFinal(bytes);
            result = new String(decoded, CHARSET);
        } catch (Exception e) {
            logger.error("AesUtils Aes256Decode error:", e);
        }
        return result;
    }

    public static String Aes256DecodeWithBase64(String str, String key) {
        if (StringUtils.isBlank(str) || StringUtils.isBlank(key)) {
            return null;
        }
        String result = null;
        try {
            Cipher cipher = Cipher.getInstance(ALGORITHM, "BC");
            SecretKeySpec keySpec = new SecretKeySpec(key.getBytes(CHARSET), "AES"); // 生成加密解密需要的Key
            cipher.init(Cipher.DECRYPT_MODE, keySpec);
            byte[] decoded = cipher.doFinal(Base64.decodeBase64(str.getBytes(CHARSET)));
            result = new String(decoded, CHARSET);
        } catch (Exception e) {
            logger.error("AesUtils Aes256Decode error:", e);
        }
        return result;
    }

    /**
     * 网联敏感字段密文解密--主要用于解密对账明细文件
     *
     * @param bytes 要被解密的密文.getByte
     * @param key   AES密钥.getByte
     * @return 解密后的byte数组
     */
    public static byte[] Aes256DecodeToByte(byte[] bytes, byte[] key) {
        try {
            Cipher cipher = Cipher.getInstance(ALGORITHM, "BC");
            SecretKeySpec keySpec = new SecretKeySpec(key, "AES"); // 生成加密解密需要的Key
            cipher.init(Cipher.DECRYPT_MODE, keySpec);
            byte[] decoded = cipher.doFinal(bytes);
            return decoded;
        } catch (Exception e) {
            logger.error("Aes256DecodeToByte error:", e);
            return null;
        }
    }

    public static void main(String[] args) {
//    	String string = UUID.randomUUID().toString().replace("-", "");
//    	System.out.println(string);
        String base64 = Aes256EncodeWithBase64("6217002710000684874", "a5100ed9789b4881bad3db440c45fb53");
        //8b7ff01f8658495b8a38ecc1329bc368  DWfmc/eGJ2LwEC/wPnP0PfURLv0RrDzfk00o1voT0SI=
        System.out.println(base64);
    }

}
