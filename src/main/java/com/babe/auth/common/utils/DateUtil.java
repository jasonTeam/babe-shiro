package com.babe.auth.common.utils;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * 日期工具类集合
 * create by szy
 * 2019-12-01
 */
public class DateUtil {

    public static final String FULL_TIME_PATTERN = "yyyyMMddHHmmss";

    public static final String FULL_TIME_SPLIT_PATTERN = "yyyy-MM-dd HH:mm:ss";

    public static final String CST_TIME_PATTERN = "EEE MMM dd HH:mm:ss zzz yyyy";

    /**
     * 获取当前时间戳 (单位: 秒)
     * @return
     */
    public static long getCurrentTimestamp(){
        return System.currentTimeMillis()/1000;
    }

    /**
     * 获取当前时间戳 (单位: 毫秒)
     * @return
     */
    public static long getCurrentTimestampMs(){
        return System.currentTimeMillis();
    }

    /**
     * 获取yyyyMMddHHmmss的当时时间格式
     * @return
     */
    public static String getCurrentDateTime() {
        return formatCurrentDateTime("yyyyMMddHHmmss");
    }

    /**
     * 根据传进来的格式，将时间进行格式化并返回
     * @return
     */
    public static String formatCurrentDateTime(String formatText) {
        Calendar calendar = Calendar.getInstance();
        return convertDateToStr(calendar.getTime(), formatText);
    }

    /**
     * 将日期格式进行转化，返回字符串格式
     * @param date
     * @param pattern
     * @return
     */
    public static String convertDateToStr(Date date, String pattern) {
        if (date == null)
            return null;
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.format(date);
    }

    public static String getDateFormat(Date date, String dateFormatType) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormatType, Locale.CHINA);
        return simpleDateFormat.format(date);
    }

    public static String formatCSTTime(String date, String format) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(CST_TIME_PATTERN, Locale.US);
        Date usDate = simpleDateFormat.parse(date);
        return DateUtil.getDateFormat(usDate, format);
    }

    public static String formatFullTime(LocalDateTime localDateTime, String pattern) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(pattern);
        return localDateTime.format(dateTimeFormatter);
    }


    //测试
    public static void main(String[] args){
        System.out.println(DateUtil.getCurrentTimestampMs());
    }


}
