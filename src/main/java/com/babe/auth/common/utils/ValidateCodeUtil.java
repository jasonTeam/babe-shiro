package com.babe.auth.common.utils;

import com.babe.auth.common.exception.BabeException;
import com.babe.auth.common.redis.RedisService;
import com.babe.auth.common.results.ResultCode;
import com.wf.captcha.GifCaptcha;
import com.wf.captcha.SpecCaptcha;
import com.wf.captcha.base.Captcha;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Description 图形验证码工具类
 * Datetime: 2020/2/14 18:07
 * Author:jason
 */
@Service
public class ValidateCodeUtil {

    @Autowired
    private RedisService redisService;


    public void createValidateCode(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();
        String key = session.getId();

        setHeader(response,"png");
        Captcha captcha = createCaptcha();
        //存入redis
        redisService.set("babe_captcha_"  + key, StringUtils.lowerCase(captcha.text()),120L);
        captcha.out(response.getOutputStream());
    }

    /**
     * 创建图形验证码
     * @return
     */
    private Captcha createCaptcha() {
        Captcha captcha = null;
        captcha = new SpecCaptcha(130,48,4);
        captcha.setCharType(2);
        return captcha;
    }

    /**
     * 图形验证码头
     */
    private void setHeader(HttpServletResponse response, String type) {
        if(StringUtils.equalsIgnoreCase(type,"gif")){
            response.setContentType(MediaType.IMAGE_GIF_VALUE);
        }else{
            response.setContentType(MediaType.IMAGE_PNG_VALUE);
        }
        response.setHeader(HttpHeaders.PRAGMA,"No-cache");
        response.setHeader(HttpHeaders.CACHE_CONTROL,"No-cache");
        response.setDateHeader(HttpHeaders.EXPIRES,0L);
    }

    /**
     * 校验图形验证码
     * @param key
     * @param value
     */
    public void checkCode(String key,String value){
        Object codeInRedis = redisService.get("babe_captcha_" + key);
        if(StringUtils.isBlank(value)){
            throw new BabeException(ResultCode.IMAGE_CODE_NOT_NULL.message());
        }
        if(codeInRedis == null){
            throw new BabeException(ResultCode.IMAGE_CODE_EXPIRE.message());
        }
        if(!StringUtils.equalsIgnoreCase(value,String.valueOf(codeInRedis))){
            throw new BabeException(ResultCode.IMAGE_CODE_ERROR.message());
        }
    }



}
