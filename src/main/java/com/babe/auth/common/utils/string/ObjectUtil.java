package com.babe.auth.common.utils.string;

import java.math.BigDecimal;
import java.util.Map;

public class ObjectUtil {

    //private static final int BUFFER_SIZE = 16 * 1024 ;
    public static boolean isNone(String st) {
        return st == null || st.equals("");
    }

    public static String getString(String st) {
        return st == null ? "" : st.trim();
    }

    public static int getInt(String st) {
        return getDouble(st).intValue();
    }

    public static int getInt(Object st) {
        return getDouble(st).intValue();
    }

    public static Long getLong(Object obj) {
        return getDouble(obj).longValue();
    }

    public static float getFloat(String st) {
        return st == null || (st.trim()).equals("") ? 0f : Float.parseFloat(st.trim());
    }

    public static Double getDouble(String st) {
        return st == null || (st.trim()).equals("") || "null".equalsIgnoreCase(st) ? 0d : Double.parseDouble(st.trim());
    }

    public static Double getDouble(Object obj) {
        return getDouble(obj == null ? null : obj.toString().trim());
    }

    public static String getString(Object o) {
        return o == null ? "" : o.toString();
    }

    public static boolean isEmpty(Object obj) {
        if (obj == null) {
            return true;
        } else if (obj instanceof String) {
            if ("".equals(((String) obj).trim())) {
                return true;
            }
        } else if (obj instanceof Object[]) {
            if (((Object[]) obj).length == 0) {
                return true;
            }
        }

        return false;
    }

    public static boolean isNotEmpty(Object obj) {
        if (obj == null) {
            return false;
        } else if (obj instanceof String) {
            if ("".equals(((String) obj).trim())) {
                return false;
            }
        } else if (obj instanceof Object[]) {
            if (((Object[]) obj).length == 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * 转换为BigDecimal 并保留两位小数
     *
     * @param obj
     * @return
     */
    public static BigDecimal getBigDecimal(Object obj) {
        obj = obj == null ? "0" : obj.toString();
        return new BigDecimal(obj.toString()).setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    /**
     * 转换为BigDecimal
     *
     * @param obj 要转换的内容
     * @param len 要保留的小数位
     * @return
     */
    public static BigDecimal getBigDecimal(Object obj, int len) {
        Double o = getDouble(obj);
        return new BigDecimal(o).setScale(len, BigDecimal.ROUND_HALF_UP);
    }

    public static void copyMap(Map<String, Object> srcMap, Map<String, Object> tagMap, String... keys) {
        for (String key : keys) {
            tagMap.put(key, srcMap.get(key));
        }
    }
}
