package com.babe.auth.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.List;

/**
 * Description 参数按字典顺序排序
 * Datetime: 2020/3/14 22:03
 * Author:jason
 */
public class ParamSortUtil {

    private static Logger log = LoggerFactory.getLogger(ParamSortUtil.class);

    public static String dicSort(Map<String, Object> paramsMap, String key) {
        List<Map.Entry<String, Object>> infoIds = new ArrayList<Map.Entry<String, Object>>(paramsMap.entrySet());
        Collections.sort(infoIds, new Comparator<Map.Entry<String, Object>>() {
            public int compare(Map.Entry<String, Object> arg0, Map.Entry<String, Object> arg1) {
                return (arg0.getKey()).compareTo(arg1.getKey());
            }
        });
        String ret = "";
        for (Map.Entry<String, Object> entry : infoIds) {
            ret += entry.getKey();
            ret += "=";
            ret += entry.getValue();
            ret += "&";
        }
        ret = ret.substring(0, ret.length() - 1);
        log.info("请求参数按照名称字符升序排列后为:[{}]", ret);
        String retStr = ret + "&key=" + key;
        return retStr;
    }

    public static void main(String[] args){
        Map<String,Object> paraMap = new HashMap<>();
        paraMap.put("c","jason");
        paraMap.put("a","12345");
        paraMap.put("e","1526715149");
        paraMap.put("b",100);
        String key = "fadfa1213afasfads";
        System.out.println(ParamSortUtil.dicSort(paraMap,key));
    }




}
