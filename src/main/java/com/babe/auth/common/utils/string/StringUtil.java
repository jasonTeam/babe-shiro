package com.babe.auth.common.utils.string;

import org.apache.commons.lang3.StringUtils;

/**
 * Description 字符串工具类
 * Datetime: 2020/3/14 21:00
 * Author:jason
 */
public class StringUtil {

    public static final boolean isNotEmpty(Object obj) {
        return !isEmpty(obj);
    }


    public static final boolean isEmpty(Object obj) {
        if (obj == null) {
            return true;
        } else if ("null".equalsIgnoreCase(obj.toString())) {
            return true;
        } else if (StringUtils.isBlank(obj.toString())) {
            return true;
        }
        return StringUtils.isEmpty(obj.toString());
    }


}
