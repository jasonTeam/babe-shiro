package com.babe.auth.common.utils.money;

import com.babe.auth.common.utils.string.StringUtil;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * Description 金额转换工具类
 * Datetime: 2020/3/14 20:54
 * Author:jason
 */
public class MoneyUtil {

    private static Logger log = LoggerFactory.getLogger(MoneyUtil.class);
    /**
     * 金额 元 格式转换对象
     */
    public static DecimalFormat FORMAT_YUAN = new DecimalFormat("0.00");

    /**
     * 精确到小数位 4
     */
    public static DecimalFormat FORMAT_DECIMALS_4 = new DecimalFormat("0.0000");

    /**
     * 金额 分 格式转换对象
     */
    public static DecimalFormat FORMAT_FEN = new DecimalFormat("0");

    /**
     * 转成 分
     *
     * @param obj
     * @return
     */
    public static Double toDoubleCent(Object obj) {
        if (obj == null) {
            obj = 0;
        }
        return NumberUtils.toDouble(
                BigDecimal.valueOf(NumberUtils.toDouble(obj.toString())).multiply(new BigDecimal(100)).toString());
    }

    /**
     * 转成 分
     *
     * @param obj
     * @return
     */
    public static String toStrCent(Object obj) {
        String cent = formatCent(toDoubleCent(obj));
        return cent == null || "0".equals(cent) ? "" : cent;
    }

    /**
     * 转成 分 原值要是为0则保留
     *
     * @param obj
     * @return
     */
    public static String toStrCentKeepZero(Object obj) {
        if (StringUtil.isNotEmpty(obj) && 0 == Double.parseDouble(obj.toString())) {
            return obj.toString();
        }
        String cent = formatCent(toDoubleCent(obj));
        return cent == null || "0".equals(cent) ? "" : cent;
    }

    /**
     * 转成 元
     *
     * @param obj
     * @return
     */
    public static Double toDoubleYuan(Object obj) {
        if (obj == null) {
            obj = 0;
        }
        return NumberUtils.toDouble(
                BigDecimal.valueOf(NumberUtils.toDouble(obj.toString())).divide(new BigDecimal(100)).toString());
    }

    /**
     * 转成 元
     *
     * @param obj
     * @return
     */
    public static String toStrYuan(Object obj) {
        String yuan = formatYuan(toDoubleYuan(obj));
        return yuan == null || "0.00".equals(yuan) ? "" : yuan;
    }

    /**
     * 转成 元原值要是为0则保留
     *
     * @param obj
     * @return
     */
    public static String toStrYuanKeepZero(Object obj) {
        if (StringUtil.isNotEmpty(obj) && 0 == Double.parseDouble(obj.toString())) {
            return obj.toString();
        }
        String yuan = formatYuan(toDoubleYuan(obj));
        return yuan == null || "0.00".equals(yuan) ? "" : yuan;
    }

    /**
     * 格式化分 ，变成没有小数点的数字字符串
     *
     * @param obj
     * @return
     */
    private static String formatCent(Object obj) {
        return FORMAT_FEN.format(obj);
    }

    /**
     * 格式化元 ，变成两位小数点的数字字符串
     *
     * @param obj
     * @return
     */
    public static String formatYuan(Object obj) {
        if (obj == null) {
            return "0.00";
        }
        if (obj.getClass().isAssignableFrom(String.class)) {
            obj = Double.parseDouble(obj.toString());
        }
        return FORMAT_YUAN.format(obj);
    }

    /**
     * 计算费用，最低收取 1分 [提供精确的小数位四舍五入处理,保留2位小数。 ]
     *
     * @param rate  点数
     * @param txamt 分
     * @param low   分
     * @return 分
     */
    public static int calculate(String rate, String txamt, double low) {
        log.info("手续费计算,小数进位机制[四省五入] 费率[{}] 金额[{}] 最小金额[{}]", rate, txamt, low);
        double rate_ = toDoubleYuan(rate);
        double txamt_ = toDoubleYuan(txamt);

        Double tmpTxAmt = mul(rate_, txamt_);
        Double tmpTxAmt_ = mul(round(tmpTxAmt.doubleValue(), 2), 100.00);

        if (tmpTxAmt_.intValue() <= 0) {
            tmpTxAmt_ = low;
        }

        return tmpTxAmt_.intValue();
    }

    /**
     * 计算费用[小数位>0时自动进一位,比如用于交易手续费计算]
     *
     * @param rate  点数
     * @param txamt 分
     * @param low   最低手续费 分
     * @return 分
     */
    public static int calculateUp(String rate, String txamt, double low) {
        log.info("手续费计算,小数进位机制[进位:下一位小数非0进1] 费率[{}] 金额[{}] 最小金额[{}]", rate, txamt, low);
        double rate_ = toDoubleYuan(rate);
        double txamt_ = toDoubleYuan(txamt);

        Double tmpTxAmt = mul(rate_, txamt_);
        Double tmpTxAmt_ = mul(round(tmpTxAmt.doubleValue(), 2, BigDecimal.ROUND_UP), 100.00);

        if (tmpTxAmt_.intValue() <= 0) {
            tmpTxAmt_ = low;
        }

        return tmpTxAmt_.intValue();
    }

    /**
     * 计算费用 [小数位自动忽略,比如用于分润计算]
     *
     * @param rate  点数
     * @param txamt 分
     * @param low   分
     * @return 分
     */
    public static int calculateDown(String rate, String txamt, double low) {
        log.info("手续费计算,小数进位机制[舍弃:舍弃下一位小数] 费率[{}] 金额[{}] 最小金额[{}]", rate, txamt, low);
        double rate_ = toDoubleYuan(rate);
        double txamt_ = toDoubleYuan(txamt);

        Double tmpTxAmt = mul(rate_, txamt_);
        Double tmpTxAmt_ = mul(round(tmpTxAmt.doubleValue(), 2, BigDecimal.ROUND_DOWN), 100.00);

        if (tmpTxAmt_.intValue() <= 0) {
            tmpTxAmt_ = low;
        }

        return tmpTxAmt_.intValue();
    }

    /**
     * Double相乘
     *
     * @param v1
     * @param v2
     * @return
     */
    public static Double mul(Double v1, Double v2) {
        BigDecimal b1 = new BigDecimal(v1.toString());
        BigDecimal b2 = new BigDecimal(v2.toString());
        return new Double(b1.multiply(b2).doubleValue());
    }

    /**
     * 提供精确的小数位四舍五入处理。
     *
     * @param v     需要四舍五入的数字
     * @param scale 小数点后保留几位
     * @return 四舍五入后的结果
     */
    public static double round(double v, int scale) {
//		BigDecimal b = new BigDecimal(Double.toString(v));
//		return b.divide(new BigDecimal("1"), scale, BigDecimal.ROUND_HALF_UP).doubleValue();

        return round(v, scale, BigDecimal.ROUND_HALF_UP);
    }

    /**
     * 提供精确的小数位处理舍/入操作
     *
     * @param v     需要处理的数字
     * @param scale 小数点后保留几位
     * @return
     */
    public static double round(double v, int scale, int roundType) {
        BigDecimal b = new BigDecimal(Double.toString(v));
        return b.divide(new BigDecimal("1"), scale, roundType).doubleValue();
    }

    public static void main(String[] args) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("a", null);
        System.out.println(MoneyUtil.toDoubleCent(map.get("a")));
        String rate = "0.005";
        String amt = "1000";
        double low = 1;
        int fee = calculateDown(rate, amt, low);
        System.out.println(fee);

        System.out.println(toDoubleYuan(rate).floatValue());
        System.out.println(calculateUp(rate, "10000", 0));

        BigDecimal b = new BigDecimal(0.000050);
        System.out.println(b.setScale(6, BigDecimal.ROUND_HALF_UP));

        System.out.println(Double.valueOf("0.0005").doubleValue());
    }



}
