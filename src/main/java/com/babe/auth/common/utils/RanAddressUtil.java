package com.babe.auth.common.utils;

import java.util.Random;

/**
 * Description 随机地址工具类
 * Datetime: 2020/3/14 21:20
 * Author:jason
 */
public class RanAddressUtil {

    /**
     * 得到随机地址
     * 地址格式：省-市-区-街道
     *
     * @return
     */
    public String getAddress() {
        Random r = new Random();
        String[] adslist = addressList();
        return adslist[r.nextInt(40)];
    }

    /**
     * 地址列表
     *
     * @return
     */
    public String[] addressList() {
        StringBuffer adds = new StringBuffer("");
        String strEnd = "-市辖区,";
        adds.append("湖北省-武汉市-汉阳区" + strEnd);
        adds.append("湖北省-武汉市-江夏区" + strEnd);
        adds.append("湖北省-武汉市-洪山区" + strEnd);
        adds.append("陕西省-安康市-汉滨区" + strEnd);
        adds.append("陕西省-安康市-紫阳区" + strEnd);
        adds.append("安徽省-安庆市-普陀区" + strEnd);
        adds.append("安徽省-蚌埠市-杨浦区" + strEnd);
        adds.append("辽宁省-鞍山市-铁东区" + strEnd);
        adds.append("四川省-巴中市-樊城区" + strEnd);
        adds.append("甘肃省-白银市-白银区" + strEnd);
        adds.append("广西壮族自治区-百色市-樊城区" + strEnd);
        adds.append("内蒙古自治区-包头市-金台区" + strEnd);
        adds.append("安徽省-安庆市-嘉定区" + strEnd);
        adds.append("河北省保定市-满城区" + strEnd);
        adds.append("北京-北京市-大兴区" + strEnd);
        adds.append("北京-北京市-顺义区" + strEnd);
        adds.append("北京-北京市-海淀区" + strEnd);
        adds.append("湖南省-常德市-永定区" + strEnd);
        adds.append("江苏省-常州市-钟楼区" + strEnd);
        adds.append("湖南省-郴州市-北湖区" + strEnd);
        adds.append("四川省-成都市-龙泉驿区" + strEnd);
        adds.append("四川省-成都市-武侯区" + strEnd);
        adds.append("河北省-承德市-竞秀区" + strEnd);
        adds.append("辽宁省-大连市-沙河口区" + strEnd);
        adds.append("广东省-东莞市-湘桥区" + strEnd);
        adds.append("山东省-东营市-武昌区" + strEnd);
        adds.append("广东省-佛山市-黄州区" + strEnd);
        adds.append("福建省-福州市-晋安区" + strEnd);
        adds.append("福建省-福州市-仓山区" + strEnd);
        adds.append("江西省-赣州市-武侯区" + strEnd);
        adds.append("宁夏回族自治区-固原市-原州区" + strEnd);
        adds.append("广东省-广州市-增城区" + strEnd);
        adds.append("广西壮族自治区-贵港市-洪山区" + strEnd);
        adds.append("贵州省-贵阳市-白云区" + strEnd);
        adds.append("黑龙江省-哈尔滨市-桃山区" + strEnd);
        adds.append("青海省-海西蒙古族藏族自治州-甘州区" + strEnd);
        adds.append("浙江省-杭州市-西湖区" + strEnd);
        adds.append("云南省-红河哈尼族彝族自治州-五华区" + strEnd);
        adds.append("江苏省-淮安市-淮阴区" + strEnd);
        adds.append("上海-上海市-宝山区" + strEnd);
        String[] adsAry = adds.toString().split(",");
        return adsAry;
    }

    public static void main(String[] args) {
        String str = new RanAddressUtil().getAddress();
        System.out.println(str);
    }


}
