package com.babe.auth.common.utils.tuomin;

/**
 * Description 托敏工具类
 * Datetime: 2020/3/14 21:10
 * Author:jason
 */
public class TuoMinUtil {

    /**
     * 银行卡号脱敏
     *
     * @param bankAccount
     * @return
     */
    public static String getCardNo(String bankAccount) {
        int length = bankAccount.length();
        String cno = bankAccount.substring(0, length - 10) + "******" + bankAccount.substring(length - 4);
        StringBuilder printcno = new StringBuilder();
        for (int i = 0; i < cno.length(); i++) {
            if (i != 0 && i % 4 == 0) {
                printcno.append(" ");
            }
            printcno.append(cno.substring(i, i + 1));
        }
        return printcno.toString();
    }


    public static void main(String[] agrs){
        String bankNo = "6222600260001072444";
        System.out.println(TuoMinUtil.getCardNo(bankNo));
    }



}
