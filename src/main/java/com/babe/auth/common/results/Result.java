package com.babe.auth.common.results;

import org.springframework.http.HttpStatus;

/**
 * Description
 * Datetime: 2020/2/12 23:26
 * Author:jason
 */
public class Result {

    private Integer code;
    private String message;
    private Object data;

    //返回成功
    public static Result success(){
        Result result = new Result();
        result.setCode(ResultCode.SUCCESS.code());
        result.setMessage(ResultCode.SUCCESS.message());
        return result;
    }

    //返回成功
    public static Result success(Object data){
        Result result = new Result();
        result.setCode(ResultCode.SUCCESS.code());
        result.setMessage(ResultCode.SUCCESS.message());
        result.setData(data);
        return result;
    }


    //返回失败
    public static Result failure(ResultCode resultCode){
        Result result = new Result();
        result.setCode(resultCode.code());
        result.setMessage(resultCode.message());
        return result;
    }

    /**
     * 返回失败
     * @param resultCode
     * @param message
     * @return
     */
    public static Result failure(ResultCode resultCode,String message){
        Result result = new Result();
        result.setCode(resultCode.code());
        result.setMessage(message);
        return result;
    }


    //返回失败
    public static Result failure(ResultCode resultCode, Object data){
        Result result = new Result();
        result.setCode(resultCode.code());
        result.setMessage(resultCode.message());
        result.setData(data);
        return result;
    }


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }


}
