package com.babe.auth.common.shiro;

import com.babe.auth.common.enums.UserEnum;
import com.babe.auth.common.exception.BabeException;
import com.babe.auth.common.results.ResultCode;
import com.babe.auth.system.Service.MenuService;
import com.babe.auth.system.Service.RoleService;
import com.babe.auth.system.Service.UserService;
import com.babe.auth.system.entity.po.Role;
import com.babe.auth.system.entity.po.User;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Description 自定义实现 ShiroRealm，包含认证和授权两大模块
 * Datetime: 2020/2/13 18:30
 * Author:jason
 */
//@Component
public class MyRealm extends AuthorizingRealm {

    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private MenuService menuService;


    /**
     * 授权方法
     * @param principal
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principal) {
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        String userName = user.getUsername();

        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        //获取用户角色集合
        List<Role> roleList = roleService.listRolesByUserName(userName);
        Set<String> setRole = new HashSet<>();
        for(Role role : roleList){
            setRole.add(role.getRoleName());
        }
        info.setRoles(setRole);

        //获取用户菜单权限集合
        List<String> menuPermList = menuService.listMenuPermsByUserName(userName);
        Set<String> setMenuPerm = new HashSet<>();
        for(String menuPerm : menuPermList){
            setMenuPerm.add(menuPerm);
        }
        info.setStringPermissions(setMenuPerm);
        return info;
    }

    /**
     * 认证方法
     * @param authToken
     * @return
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authToken)throws AuthenticationException{
        //获取用户输入的用户名和密码
        String userName = String.valueOf(authToken.getPrincipal());
        String password = new String((char[]) authToken.getCredentials());

        //通过用户名获取用户信息
        User user = userService.getUserByUserName(userName);
        if(null == user){
            throw new UnknownAccountException(ResultCode.USER_NOT_EXIST.message());
        }
        if(!StringUtils.equals(password,user.getPassword())){
            throw new IncorrectCredentialsException(ResultCode.USER_LOGIN_ERROR.message());
        }
        if(StringUtils.equals(UserEnum.USER_STATUS_LOCK.value(),user.getStatus())){
            throw new LockedAccountException(ResultCode.USER_ACCOUNT_FORBIDDEN.message());
        }
        return new SimpleAuthenticationInfo(user, password, getName());
    }

    /**
     * 清除当前用户权限缓存
     * 使用方法：在需要清除用户权限的地方注入 ShiroRealm,
     * 然后调用其 clearCache方法。
     */
    public void clearCache() {
        PrincipalCollection principals = SecurityUtils.getSubject().getPrincipals();
        super.clearCache(principals);
    }



}
