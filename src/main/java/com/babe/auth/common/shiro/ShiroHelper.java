package com.babe.auth.common.shiro;

import org.apache.shiro.authz.AuthorizationInfo;
import org.springframework.stereotype.Component;

/**
 * Description
 * Datetime: 2020/2/14 15:15
 * Author:jason
 */
@Component
public class ShiroHelper extends MyRealm{

    /**
     * 获取当前用户的角色和权限集合
     *
     * @return AuthorizationInfo
     */
    public AuthorizationInfo getCurrentuserAuthorizationInfo() {
        return super.doGetAuthorizationInfo(null);
    }


}
