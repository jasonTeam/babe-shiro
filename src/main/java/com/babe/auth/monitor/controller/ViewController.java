package com.babe.auth.monitor.controller;

import com.babe.auth.common.utils.BabeUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Description 监控模块
 * Datetime: 2020/2/23 13:59
 * Author:jason
 */

@Controller("monitorView")
@RequestMapping("babe/views/" + "monitor")
public class ViewController {

//    @Autowired
//    private FebsActuatorHelper actuatorHelper;

    @GetMapping("online")
    @RequiresPermissions("online:view")
    public String online() {
        return BabeUtil.view("monitor/online");
    }

    @GetMapping("log")
    @RequiresPermissions("log:view")
    public String log() {
        return BabeUtil.view("monitor/log");
    }

    @GetMapping("loginlog")
    @RequiresPermissions("loginlog:view")
    public String loginLog() {
        return BabeUtil.view("monitor/loginLog");
    }

    @GetMapping("httptrace")
    @RequiresPermissions("httptrace:view")
    public String httptrace() {
        return BabeUtil.view("monitor/httpTrace");
    }

    @GetMapping("jvm")
    @RequiresPermissions("jvm:view")
    public String jvmInfo(Model model) {
//        List<FebsMetricResponse> jvm = actuatorHelper.getMetricResponseByType("jvm");
//        JvmInfo jvmInfo = actuatorHelper.getJvmInfoFromMetricData(jvm);
//        model.addAttribute("jvm", jvmInfo);
        return BabeUtil.view("monitor/jvmInfo");
    }

    @GetMapping("tomcat")
    @RequiresPermissions("tomcat:view")
    public String tomcatInfo(Model model) {
//        List<FebsMetricResponse> tomcat = actuatorHelper.getMetricResponseByType("tomcat");
//        TomcatInfo tomcatInfo = actuatorHelper.getTomcatInfoFromMetricData(tomcat);
//        model.addAttribute("tomcat", tomcatInfo);
        return BabeUtil.view("monitor/tomcatInfo");
    }

    @GetMapping("server")
    @RequiresPermissions("server:view")
    public String serverInfo(Model model) {
//        List<FebsMetricResponse> jdbcInfo = actuatorHelper.getMetricResponseByType("jdbc");
//        List<FebsMetricResponse> systemInfo = actuatorHelper.getMetricResponseByType("system");
//        List<FebsMetricResponse> processInfo = actuatorHelper.getMetricResponseByType("process");
//
//        ServerInfo serverInfo = actuatorHelper.getServerInfoFromMetricData(jdbcInfo, systemInfo, processInfo);
//        model.addAttribute("server", serverInfo);
        return BabeUtil.view("monitor/serverInfo");
    }

    @GetMapping("swagger")
    public String swagger() {
        return BabeUtil.view("monitor/swagger");
    }



}
