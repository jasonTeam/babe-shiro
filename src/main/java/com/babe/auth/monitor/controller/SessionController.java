package com.babe.auth.monitor.controller;

import com.babe.auth.common.results.Result;
import com.babe.auth.monitor.entity.OnlineUser;
import com.babe.auth.monitor.service.SessionService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Description
 * Datetime: 2020/3/6 15:44
 * Author:jason
 */
@RestController
@RequestMapping("session")
public class SessionController {

    @Autowired
    private SessionService sessionService;

    /**
     * 在线用户列表
     * @param username
     * @return
     */
    @GetMapping("list")
    @RequiresPermissions("online:view")
    public Result list(String username) {
        List<OnlineUser> list = sessionService.onLineUserList(username);
        Map<String, Object> data = new HashMap<>();
        data.put("rows", list);
        data.put("total", CollectionUtils.size(list));
        return Result.success(data);
    }


    /**
     * 踢出用户
     * @param id
     * @return
     */
    @GetMapping("delete/{id}")
    @RequiresPermissions("user:kickout")
    public Result forceLogout(@PathVariable String id) {
        sessionService.kickout(id);
        return new Result().success();
    }


}
