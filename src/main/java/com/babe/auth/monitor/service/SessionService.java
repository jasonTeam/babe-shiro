package com.babe.auth.monitor.service;

import com.babe.auth.monitor.entity.OnlineUser;

import java.util.List;

/**
 * Description Session接口
 * Datetime: 2020/3/6 15:33
 * Author:jason
 */
public interface SessionService {

    /**
     * 在线用户
     * @param username
     * @return
     */
    List<OnlineUser> onLineUserList(String username);


    /**
     * 踢出用户
     *
     * @param sessionId sessionId
     */
    void kickout(String sessionId);



}
