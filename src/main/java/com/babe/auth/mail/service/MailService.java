package com.babe.auth.mail.service;

import javax.mail.MessagingException;

/**
 * Description
 * Datetime: 2020/3/22 11:45
 * Author:jason
 */

public interface MailService {

    /**
     * 发送简单邮件
     * @param to 目标邮箱
     * @param subject 主题
     * @param content 内容
     */
    void sendSimpleMail(String to,String subject,String content);

    /**
     * 发送HTML邮件 MimeMessages为复杂邮件模板，支持文本、附件、HTML、图片等。
     * @param to
     * @param subject
     * @param content
     * @throws MessagingException
     */
    void sendHtmlMail(String to,String subject,String content) throws MessagingException;


}
