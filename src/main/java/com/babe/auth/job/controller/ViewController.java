package com.babe.auth.job.controller;

import com.babe.auth.common.constant.BabeConstant;
import com.babe.auth.common.utils.BabeUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.constraints.NotBlank;

/**
 * Description
 * Datetime: 2020/3/6 15:22
 * Author:jason
 */
@Controller("jobView")
@RequestMapping(BabeConstant.VIEW_PREFIX + "job")
public class ViewController {

//    @Autowired
//    private IJobService jobService;

    @GetMapping("job")
    @RequiresPermissions("job:view")
    public String online() {
        return BabeUtil.view("job/job");
    }

    @GetMapping("job/add")
    @RequiresPermissions("job:add")
    public String jobAdd() {
        return BabeUtil.view("job/jobAdd");
    }

    @GetMapping("job/update/{jobId}")
    @RequiresPermissions("job:update")
    public String jobUpdate(@NotBlank(message = "{required}") @PathVariable Long jobId, Model model) {
//        Job job = jobService.findJob(jobId);
//        model.addAttribute("job", job);
        return BabeUtil.view("job/jobUpdate");
    }

    @GetMapping("log")
    @RequiresPermissions("job:log:view")
    public String log() {
        return BabeUtil.view("job/jobLog");
    }
        
    
}
