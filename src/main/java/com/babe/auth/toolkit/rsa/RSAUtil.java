package com.babe.auth.toolkit.rsa;



import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPublicKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.x509.RSAPublicKeyStructure;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;

import com.rd.util.base64.RdBase64Util;

/**
 * rsa签名辅助工具类
 * 官方网站：www.erongdu.com
 * 研发中心：rdc@erongdu.com
 * 未经授权不得进行修改、复制、出售及商业使用
 */
@SuppressWarnings("deprecation")
public class RSAUtil {

	/**
	 * x509格式公钥转换为Der格式
	 *
	 * @param x509PublicKey x509格式公钥字符串
	 * @return Der格式公钥字符串
	 */
	public static String getRsaPublicKeyDerFromX509(String x509PublicKey) {
		try {
			ASN1InputStream aIn = new ASN1InputStream(hexString2ByteArr(x509PublicKey));
			SubjectPublicKeyInfo info = SubjectPublicKeyInfo.getInstance(aIn.readObject());
			RSAPublicKeyStructure struct = RSAPublicKeyStructure.getInstance(info.getPublicKey());
			if (aIn != null)
				aIn.close();
			return byteArr2HexString(struct.getEncoded());
		} catch (IOException e) {
			return null;
		}
	}

	/**
	 * 从Der格式公钥生成公钥
	 *
	 * @param publicKeyDer Der格式公钥字符串
	 * @return 公钥
	 */
	public static PublicKey generatePublicKeyFromDer(String publicKeyDer) {
		try {
			ASN1InputStream aIn = new ASN1InputStream(hexString2ByteArr(publicKeyDer));
			RSAPublicKeyStructure pStruct = RSAPublicKeyStructure.getInstance(aIn.readObject());
			RSAPublicKeySpec spec = new RSAPublicKeySpec(pStruct.getModulus(), pStruct.getPublicExponent());
			KeyFactory kf = KeyFactory.getInstance("RSA");
			if (aIn != null)
				aIn.close();
			return kf.generatePublic(spec);
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * 生成私钥文件
	 * @param privateKeyStr
	 * @return
	 */
	public static PrivateKey generatePrivateKey(String privateKeyStr) {
		try {
			byte[] buffer = RSAUtil.hexString2ByteArr(privateKeyStr);
			PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(buffer);
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			PrivateKey key = keyFactory.generatePrivate(keySpec);
			return key;
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * 私钥加密
	 * @param privateKeyStr 私钥字符串
	 * @param str 加密原串
	 * @return
	 * @throws Exception
	 */
	public static String encrypt(String privateKeyStr, String str) throws Exception {
		PrivateKey privateKey = RSAUtil.generatePrivateKey(privateKeyStr);
		if (privateKey == null) {
			throw new Exception("加密私钥为空, 请设置");
		}
		Cipher cipher = null;
		try {
			// 使用默认RSA
			cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.ENCRYPT_MODE, privateKey);
			byte[] output = cipher.doFinal(str.getBytes());
			// byte数组base64编码后存在非法字符，所以需要再base64编码一次
			return RdBase64Util.base64Encode(RdBase64Util.base64Encode(output));
		} catch (NoSuchAlgorithmException e) {
			throw new Exception("无此加密算法");
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
			return null;
		} catch (InvalidKeyException e) {
			throw new Exception("加密私钥非法,请检查");
		} catch (IllegalBlockSizeException e) {
			throw new Exception("明文长度非法");
		} catch (BadPaddingException e) {
			throw new Exception("明文数据已损坏");
		}
	}
	
	/**
	 * 公钥解密
	 * @param publicKeyStr 公钥字符串
	 * @param encryptStr 密文
	 * @return
	 * @throws Exception
	 */
	public static String decrypt(String publicKeyStr, String encryptStr) throws Exception {
		// 因为密文为base64编码后数据，所以需要先base64解码
		encryptStr = RdBase64Util.base64Decode(encryptStr);
		PublicKey publicKey = RSAUtil.generatePublicKeyFromDer(publicKeyStr);
		if (publicKey == null) {
			throw new Exception("解密公钥为空, 请设置");
		}
		Cipher cipher = null;
		try {
			// 使用默认RSA
			cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.DECRYPT_MODE, publicKey);
			byte[] output = cipher.doFinal(RdBase64Util.base64DecodeToArray(encryptStr));
			return new String(output);
		} catch (NoSuchAlgorithmException e) {
			throw new Exception("无此解密算法");
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
			return null;
		} catch (InvalidKeyException e) {
			throw new Exception("解密公钥非法,请检查");
		} catch (IllegalBlockSizeException e) {
			throw new Exception("密文长度非法");
		} catch (BadPaddingException e) {
			throw new Exception("密文数据已损坏");
		}
	}

	/**
	 * 字节数组转换为十六进制字符串
	 *
	 * @param byteArr 字节数组
	 * @return 十六进制字符串
	 */
	public static String byteArr2HexString(byte[] byteArr) {
		if (byteArr == null) {
			return "null";
		}
		StringBuffer sb = new StringBuffer();

		for (int k = 0; k < byteArr.length; k++) {
			if ((byteArr[k] & 0xFF) < 16) {
				sb.append("0");
			}
			sb.append(Integer.toString(byteArr[k] & 0xFF, 16));
		}
		return sb.toString();
	}

	/**
	 * 十六进制字符串转换为字节数组
	 *
	 * @param hexString 十六进制字符串
	 * @return 字节数组
	 */
	public static byte[] hexString2ByteArr(String hexString) {
		if ((hexString == null) || (hexString.length() % 2 != 0)) {
			return new byte[0];
		}

		byte[] dest = new byte[hexString.length() / 2];

		for (int i = 0; i < dest.length; i++) {
			String val = hexString.substring(2 * i, 2 * i + 2);
			dest[i] = (byte) Integer.parseInt(val, 16);
		}
		return dest;
	}
}
