package com.babe.auth.toolkit.other;

import com.babe.auth.toolkit.code.BASE64Decoder;
import com.babe.auth.toolkit.code.BASE64Encoder;

import java.io.IOException;


public class Base64Util {
	/**
	 * 字符串Base64位加密
	 * @param str
	 * @return
	 */
	public static String base64Encode(String str) {
		BASE64Encoder encoder = new BASE64Encoder();
		String result = encoder.encode(str.getBytes());
		return result;
	}
	
	/**
	 * 字符串Base64位解密
	 * @param str
	 * @return
	 * @throws IOException
	 */
	public static String base64Decode(String str) {
		try {
			if (StringUtil.isNotBlank(str)) {
				BASE64Decoder decoder = new BASE64Decoder();
				String result = decoder.decodeStr(str);
				return result;
			} else {
				return null;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}
}
