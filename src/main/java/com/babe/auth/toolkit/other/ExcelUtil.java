package com.babe.auth.toolkit.other;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

//import jxl.Cell;
//import jxl.Sheet;
//import jxl.Workbook;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class ExcelUtil {
	
//	public static final String UID = "serialVersionUID";
//	/** 上传条数*/
//	public static final int ROWS = 1000;
//	public static final String MSG = "excel条数不能超过"+ROWS+"条！";
//	private static final Logger LOGGER = Logger.getLogger(ExcelUtil.class);
//
//	@SuppressWarnings("rawtypes")
//	public static void writeExcel(String file, List list, Class clazz) throws Exception {
//		Field[] fields = clazz.getDeclaredFields();
//		List<String> flist = new ArrayList<String>();
//		for (int i = 0; i < fields.length; i++) {
//			if (fields[i].getName().equals(UID)) {
//				continue;
//			}
//			flist.add(fields[i].getName());
//		}
//		writeExcel(file, list, clazz, flist, null);
//	}
//
//	@SuppressWarnings("rawtypes")
//	public static void writeExcel(String file, List list, Class clazz, List<String> fields, List<String> titles)
//		throws Exception {
//		OutputStream os = getOutputStream(file);
//		jxl.write.WritableWorkbook wwb = Workbook.createWorkbook(os);
//		jxl.write.WritableSheet ws = wwb.createSheet("Sheet1", 0);
//		jxl.write.Label label = null;
//		int start = 0;
//		if (titles != null && titles.size() > 0) {
//			for (int j = 0; j < titles.size(); j++) {
//				label = new jxl.write.Label(j, 0, titles.get(j));
//				ws.addCell(label);
//			}
//			start++;
//		}
//		for (int i = start; i < list.size() + start; i++) {
//			Object o = list.get(i - start);
//			if (o == null) {
//				continue;
//			}
//			for (int j = 0; j < fields.size(); j++) {
//				String value = "";
//				String field = fields.get(j);
//				if (field == null || field.equals("serialVersionUID")) {
//					continue;
//				}
//				try {
//					value = ReflectUtil.invokeGetMethod(clazz, o, field).toString();
//				} catch (Exception e) {
//
//				}
//				if (field != null && isTime(field)) {
//					if (value.isEmpty()) {
//						value = "";
//					} else {
//						value = DateUtil.dateStr4(value);
//					}
//				}
//				// 判断是否包含金钱，如有，将其保留两位有效数字
//				if (field != null && isMoney(field)) {
//					if (value.isEmpty()) {
//						value = "";
//					} else {
//						value = StringUtil.isNull(BigDecimalUtil.round(value));
//					}
//				}
//				label = new jxl.write.Label(j, i, value);
//				ws.addCell(label);
//			}
//		}
//		wwb.write();
//		wwb.close();
//	}
//
//	/**
//	 *
//	  读取excel
//	 * @param file
//	 * @return
//	 * @throws Exception
//	 */
//	@SuppressWarnings("rawtypes")
//	public static List[] read(File file) throws Exception {
//		String prefix = FileTypeUtil.getFileByFile(file);
//		if("xls".equals(prefix)){
//			return readXls(file);
//		} else if ("xlsx".equals(prefix)) {
//			return readXlsx(file);
//		}
//		return new List[0];
//	}
//
//
//	/**
//	 *
//	  xls格式读取
//	 * @param file
//	 * @return
//	 * @throws Exception
//	 */
//	@SuppressWarnings("rawtypes")
//	public static List[] readXls(File file) throws Exception {
//		List[] data = null;
//		Workbook wb = null;
//		String str = "";
//		try {
//			wb = Workbook.getWorkbook(file);
//			if (null == wb) {
//				return data;
//			}
//
//			Sheet[] sheets = wb.getSheets();
//			if (null == sheets  || sheets.length == 0) {
//				return data;
//			}
//
//			int rows = sheets[0].getRows();
//			checkRows(rows);
//
//			data = new List[rows];
//			for (int j = 0; j < rows; j++) {
//				List<Object> rowData = new ArrayList<Object>();
//				Cell[] cells = sheets[0].getRow(j);
//				if (cells != null && cells.length != 0) {
//					for (int k = 0; k < cells.length; k++) {
//						String cell = cells[k].getContents();
//						rowData.add(cell);
//					}
//				}
//				data[j] = rowData;
//			}
//		} catch (Exception e) {
//			str = e.getMessage();
//			LOGGER.error(e.getMessage());
//		} finally {
//			if (wb != null)
//				wb.close();
//		}
//
//		if (MSG.equals(str)) {
//			throw new BussinessException(MSG);
//		}
//
//		return data;
//	}
//
//	/**
//	 *
//	  xlsx格式读取
//	 * @param file
//	 * @return
//	 * @throws IOException
//	 */
//	@SuppressWarnings({ "rawtypes"})
//	public static List[] readXlsx(File file) throws IOException {
//		InputStream is = null;
//		List[] data = null;
//		String str = "";
//		try {
//			is = new FileInputStream(file);
//			XSSFWorkbook xssfWorkbook = new XSSFWorkbook(is);
//			// Read the Sheet
//			XSSFSheet xssfSheet = xssfWorkbook.getSheetAt(0);
//			if (xssfSheet == null) {
//				return null;
//			}
//			int rowNums = xssfSheet.getPhysicalNumberOfRows();
//			checkRows(rowNums);
//			data = new List[rowNums];
//			//记录有效条数
//			int num = 0;
//			for (int rowNum = 0; rowNum < rowNums; rowNum++) {
//				XSSFRow xssfRow = xssfSheet.getRow(rowNum);
//				if (xssfRow == null) {
//					rowNums++;
//					continue;
//				}
//				List<Object> rowData = new ArrayList<Object>();
//				for (int k = 0; k < xssfRow.getLastCellNum(); k++) {
//					rowData.add(cell(xssfRow.getCell(k)));
//				}
//				data[num] = rowData;
//				num ++;
//			}
//		} catch (Exception e) {
//			str = e.getMessage();
//			LOGGER.error(e.getMessage());
//		} finally {
//			if (is != null) {
//				is.close();
//			}
//		}
//
//		if (MSG.equals(str)) {
//			throw new BussinessException(MSG);
//		}
//
//		return data;
//	}
//
//
//	private static Object cell(XSSFCell cell) {
//		Object o = null;
//		switch (cell.getCellType()) {
//		case XSSFCell.CELL_TYPE_STRING:
//			o = cell.getStringCellValue();
//			break;
//		case XSSFCell.CELL_TYPE_NUMERIC:
//			o = cell.getNumericCellValue();
//			break;
//		case XSSFCell.CELL_TYPE_BOOLEAN:
//			o = cell.getBooleanCellValue();
//			break;
//		case XSSFCell.CELL_TYPE_FORMULA:
//			o = cell.getArrayFormulaRange();
//			break;
//		case XSSFCell.CELL_TYPE_ERROR:
//			o = cell.getErrorCellString();
//			break;
//		default:
//			break;
//		}
//		return o;
//	}
//
//	/**
//	 * 判断是否是时间
//	 *
//	 * @param field
//	 * @return
//	 */
//	private static boolean isTime(String field) {
//		String[] times = new String[] { "addtime", "addTime", "repay_time", "verify_time", "repay_yestime",
//				"repayment_time", "repayment_yestime", "registertime", "vip_verify_time", "full_verifytime",
//				"last_tender_time", "kefu_addtime", "realname_verify_time", "video_verify_time", "scene_verify_time",
//				"phone_verify_time", "pwd_modify_time", "vip_end_time", "add_time", "update_time",
//				"interest_start_time", "interest_end_time" };
//		boolean isTime = false;
//		for (String s : times) {
//			if (s.equals(field)) {
//				isTime = true;
//				break;
//			}
//		}
//		return isTime;
//	}
//
//	/**
//	 *
//	  校验上传的条数
//	 * @param rows
//	 */
//	public static void checkRows(int rows) {
//		if (rows > ROWS) {
//			throw new BussinessException(MSG);
//		}
//	}
//
//	/**
//	 * 判断是否是金钱
//	 *
//	 * @param field
//	 * @return
//	 */
//	private static boolean isMoney(String field) {
//		String[] money = new String[] { "sum", "use_money", "collection", "total", "no_use_money", "money" };
//		boolean isMoney = false;
//		for (String s : money) {
//			if (s.equals(field)) {
//				isMoney = true;
//				break;
//			}
//		}
//		return isMoney;
//	}
//
//	public static OutputStream getOutputStream(String file) throws Exception {
//		File f = new File(file);
//		f.createNewFile();
//		OutputStream os = new FileOutputStream(f);
//		return os;
//	}
	
}
