package com.babe.auth.toolkit.other;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.config.RequestConfig.Builder;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSON;
import com.rd.p2p.common.exception.BussinessException;

/**
 * 工具类-Http请求
 * 
 * @author xx
 * @version 2.0
 * @since 2014年1月28日
 */
public class HttpUtil {
	
	
private static Logger logger = Logger.getLogger(HttpUtil.class);
	
private static String charset = "UTF-8";
	
	/**
	 * HttpClient连接超时时间(毫秒)
	 */
	public static final int UFX_CONN_TIME = NumberUtil.getInt(PropertiesUtil.getValue("ufx_conn_time"));
	
	/**
	 * HttpClient数据传输超时时间(毫秒)
	 */
	public static final int UFX_DATA_TRANS = NumberUtil.getInt(PropertiesUtil.getValue("ufx_data_trans"));
	
	/**
	 * 根据请求参数生成List<BasicNameValuePair>
	 * @param params
	 * @return
	 */
	private static List<BasicNameValuePair> wrapParam2(String[][] params) {
		List<BasicNameValuePair> data = new ArrayList<BasicNameValuePair>();
        for (int i = 0; i < params.length; i++) {
        	data.add(new BasicNameValuePair(params[i][0], params[i][1]));
        }
        return data;
	}
	
	/**
	 * HTTP POST请求
	 * @param clientURL 请求地址
	 * @param params 参数数组
	 * @return
	 */
	public static String postClient(String clientURL,String[][] params) {
		HttpPost post = new HttpPost(clientURL);
		CloseableHttpClient client = HttpClients.createDefault();
		try {
			//参数封装
			List<BasicNameValuePair> paramsList = wrapParam2(params);
			UrlEncodedFormEntity entity = new UrlEncodedFormEntity(paramsList, charset);
			
			//设置请求和传输时长
			Builder builder = RequestConfig.custom();

			builder.setSocketTimeout(UFX_DATA_TRANS);
			builder.setConnectTimeout(UFX_CONN_TIME);
			
			RequestConfig config = builder.build();
			
			post.setEntity(entity);
			post.setConfig(config);
			//发起请求
			CloseableHttpResponse response = client.execute(post);
			HttpEntity httpEntity = response.getEntity();
			String str = "";
			if (httpEntity != null) {
				str = EntityUtils.toString(httpEntity, charset);
			}
			return str;
		} catch (Exception e) {
			throw new BussinessException("请求ufx失败");
		} finally {
			try {
				if(client!=null){
					client.close();
				}
				if(post!=null){
					post.releaseConnection();
				}
			} catch (IOException e) {
				logger.error("关闭post请求流异常",e);
			}
		}
	}
	
	/**
	 * 发起http请求，获取响应结果
	 * 
	 * @param pageURL
	 * @return
	 */
	public static String getHttpResponse(String pageURL) {
		String pageContent = "";
		BufferedReader in = null;
		InputStreamReader isr = null;
		InputStream is = null;
		HttpURLConnection huc = null;
		try {
			URL url = new URL(pageURL);
			huc = (HttpURLConnection) url.openConnection();
			is = huc.getInputStream();
			isr = new InputStreamReader(is);
			in = new BufferedReader(isr);
			String line = null;
			while (((line = in.readLine()) != null)) {
				if (line.length() == 0)
					continue;
				pageContent += line;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (is != null)
					is.close();
				if (isr != null)
					isr.close();
				if (in != null)
					in.close();
				if (huc != null)
					huc.disconnect();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return pageContent;
	}

	public static <T> T getJson(String url, Class<T> clazz) {
		HttpGet httpGet = new HttpGet(url);
		httpGet.addHeader("Accept",
				"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
		httpGet.addHeader("Connection", "Keep-Alive");
		httpGet.addHeader("User-Agent",
				"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:29.0) Gecko/20100101 Firefox/29.0");
		httpGet.addHeader("Cookie", "");

		try {
			CloseableHttpClient httpclient = HttpClients.createDefault();
			CloseableHttpResponse response = httpclient.execute(httpGet);

			if (response == null) {
				return null;
			}
			String result = EntityUtils.toString(response.getEntity());
			return JSON.parseObject(result.toString().trim(), clazz);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 发送POST请求
	 * 
	 * @author Ruohai
	 * @date 2016/07/07
	 */
	public static String sendPost(String url, String param) {
		PrintWriter out = null;
		BufferedReader in = null;
		String result = "";

		try {
			URL realUrl = new URL(url);
			// 打开和URL之间的连接
			URLConnection conn = realUrl.openConnection();
			// 设置通用的请求属性
			conn.setRequestProperty("accept", "*/*");
			conn.setRequestProperty("connection", "Keep-Alive");
			conn.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
			// 发送post请求必须设置如下两行
			conn.setDoOutput(true);
			conn.setDoInput(true);
			// 获取输出流
			out = new PrintWriter(conn.getOutputStream());
			// 发送请求参数
			out.print(param);
			out.flush();
			// 读取响应
			in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line = null;
			while ((line = in.readLine()) != null) {
				result += line;
			}

		} catch (Exception e) {
			System.out.println("发送POST请求出现异常！" + e.getMessage());
			e.printStackTrace();
		} finally {
			try {
				if (out != null) {
					out.close();
				}
				if (in != null) {
					in.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return result;
	}

}
