package com.babe.auth.toolkit.other;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * 工具类-文件上传路径以及文件模块名称
 * 
 * @author：zlh
 * @version 2.0
 * @since 2015年4月21日
 */
public final class UploadFileUtil {

	private static final Logger LOGGER= Logger.getLogger(UploadFileUtil.class);

	/** 资源属性 */
	private static Properties properties;

	/**
	 * 私有构造方法
	 */
	private UploadFileUtil() {
	}

	static {
		properties = new Properties();
		try {
			// 读取配置文件
			properties.load(UploadFileUtil.class.getClassLoader().getResourceAsStream("uploadUrl.properties"));
		} catch (IOException e) {
			e.printStackTrace();
			LOGGER.error("读取配置文件出错，请确认uploadUrl.properties文件已经放到src目录下。");
		}
	}

	/**
	 * 获取配置信息
	 * 
	 * @param key 键
	 * @return 配置信息
	 */
	public static String getMessage(String key) {
		String value = null;
		value = properties.getProperty(key);
		if (null == value || "".equals(value)) {
			LOGGER.error("没有获取指定key的值，请确认资源文件中是否存在【" + key + "】");
		}
		return value;
	}

	/**
	 * 获取配置信息
	 * 
	 * @param key 键
	 * @param param 参数
	 * @return 配置信息
	 */
	public static String getMessage(String key, Object[] param) {
		String value = getMessage(key);
		return MessageFormat.format(value, param);
	}

}
