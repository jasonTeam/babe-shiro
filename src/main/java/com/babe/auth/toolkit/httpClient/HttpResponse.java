package com.babe.auth.toolkit.httpClient;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.URL;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSON;
import com.rd.p2p.common.util.StringUtil;


/* *
 *类名：HttpResponse
 *功能：Http返回对象的封装
 *详细：封装Http返回信息
 *版本：3.3
 *日期：2011-08-17
 *说明：
 *以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 *该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
 */

public class HttpResponse {
	
	private final static Logger LOGGER = Logger.getLogger(HttpResponse.class);
	
	public static final String UTF8 = "utf-8";
	
    /**
     * 返回中的Header信息
     */
    private Header[] responseHeaders;

    /**
     * String类型的result
     */
    private String   stringResult;

    /**
     * btye类型的result
     */
    private byte[]   byteResult;

    public Header[] getResponseHeaders() {
        return responseHeaders;
    }

    public void setResponseHeaders(Header[] responseHeaders) {
        this.responseHeaders = responseHeaders;
    }

    public void setByteResult(byte[] byteResult) {
        this.byteResult = byteResult;
    }

    public byte[] getByteResult() {
		return byteResult;
	}

	public String getStringResult() throws UnsupportedEncodingException {
        if (stringResult != null) {
            return stringResult;
        }
        if (byteResult != null) {
            return new String(byteResult, UTF8);
        }
        return null;
    }

    public void setStringResult(String stringResult) {
        this.stringResult = stringResult;
    }
    
    
    
	
    /**请求方式：GET**/
    public static final String HTTP_METHOD_GET = "get";
    
    /**请求方式：POST**/
    public static final String HTTP_METHOD_POST = "post";
    
    /**请求方式https：GET**/
    public static final String HTTPS_METHOD_GET = "GET";
    
    /**请求方式https：GET：POST**/
    public static final String HTTPS_METHOD_POST = "POST";
    
    /**请求参数：签名**/
    public static final String PARAM_SIGN = "sign";
    
    /**请求参数：签名方式**/
    public static final String PARAM_SIGN_TYPE = "signType";
	
    
    
    public static String doHttpsRequest4Get(String requestUrl){
    	return doHttpsRequest(requestUrl, HTTPS_METHOD_GET, null, null);
    }
    
    public static String doHttpsRequest4Post(String requestUrl){
    	return doHttpsRequest(requestUrl, HTTPS_METHOD_POST, null, null);
    }
    
    public static String doHttpsRequest4Post(String requestUrl, File file){
    	return doHttpsRequest(requestUrl, HTTPS_METHOD_POST, null, file);
    }
    
    public static String doHttpsRequest(String requestUrl, String requestMethod){
    	return doHttpsRequest(requestUrl, requestMethod, null, null);
    }
    
    
    /**
	 * http请求
	* @Title: doHttpRequest 
	* @param requestUrl
	* @param requestMethod
	* @param outputStr
	* @return
	* JSONObject 
	* @throws
	 */
	public static String doHttpsRequest(String requestUrl, String requestMethod, String outputStr, File file){
		StringBuffer buffer = new StringBuffer();
		try {
			// 创建SSLContext对象，并使用我们指定的信任管理器初始化
			TrustManager[] tm = { new MyX509TrustManager() };
			SSLContext sslContext = SSLContext.getInstance("SSL", "SunJSSE");
			sslContext.init(null, tm, new java.security.SecureRandom());
			// 从上述SSLContext对象中得到SSLSocketFactory对象
			SSLSocketFactory ssf = sslContext.getSocketFactory();
			//新建URL对象
			URL url = new URL(requestUrl);
			HttpsURLConnection httpUrlConn = (HttpsURLConnection) url.openConnection();
			httpUrlConn.setSSLSocketFactory(ssf);
			httpUrlConn.setDoOutput(true);
			httpUrlConn.setDoInput(true);
			httpUrlConn.setUseCaches(false);
			// 设置请求方式（GET/POST）
			httpUrlConn.setRequestMethod(requestMethod);

			if ("GET".equalsIgnoreCase(requestMethod))
				httpUrlConn.connect();
			
			if(file != null){
				LOGGER.info("获取file的getName。。。。。。。"+file.getName());
				OutputStream out = new DataOutputStream(httpUrlConn.getOutputStream());  
		           
	            StringBuilder sb = new StringBuilder();   
	            sb.append("Content-Disposition: form-data;name=\"upload\";filename=\""+ file.getName() + "\"\r\n");    
	            sb.append("Content-Type:application/octet-stream\r\n\r\n");    
	              
	            byte[] data = sb.toString().getBytes();  
	            out.write(data);  
	            LOGGER.info("获取data。。。。。。。"+data);
	            DataInputStream in = new DataInputStream(new FileInputStream(file));  
	            int bytes = 0;  
	            byte[] bufferOut = new byte[1024];  
	            while ((bytes = in.read(bufferOut)) != -1) {  
	                out.write(bufferOut, 0, bytes);  
	            } 
	            in.close(); 
	            
	            out.flush();    
	            out.close(); 
			}
			  

			// 当有数据需要提交时
			if (!StringUtil.isBlank(outputStr)) {
				OutputStream outputStream = httpUrlConn.getOutputStream();
				// 注意编码格式，防止中文乱码
				outputStream.write(outputStr.getBytes("UTF-8"));
				outputStream.close();
			}

			// 将返回的输入流转换成字符串
			InputStream inputStream = httpUrlConn.getInputStream();
			InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "utf-8");
			BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

			String str = null;
			while ((str = bufferedReader.readLine()) != null) {
				buffer.append(str);
			}
			bufferedReader.close();
			inputStreamReader.close();
			// 释放资源
			inputStream.close();
			inputStream = null;
			httpUrlConn.disconnect();
			return buffer.toString();
		} catch (ConnectException ce) {
			LOGGER.error("Weixin server connection timed out.", ce);
		} catch (Exception e) {
			LOGGER.error("https request error:{}", e);
		}
		return "";
	}

	
	
    /**
     * 建立请求，以模拟远程HTTP的构造并获取处理结果
     * 如：buildRequest("", "",sParaTemp)
     * @param apiUrl 接入机构服务接入网关URL
     * @param httpRequestMethod 请求方式：GET or POST
     * @param sPara 请求参数数组
     * @return 处理结果
     */
    public static String buildRequest(String apiUrl, String httpRequestMethod,Map<String, String> sPara) {
    	String strResult = "";
    	HttpResponse response = getResponse(apiUrl, httpRequestMethod, sPara, HttpResultType.STRING);
        if (response != null) {
        	try {
        		strResult = response.getStringResult();
			} catch (Exception ex) {
				LOGGER.error("调用远程HTTP接口异常：" + ex);
			}
        }
        return strResult;
    }
    
    /**
     * 建立请求，以模拟远程HTTP的构造并获取处理结果
     * 如：buildRequest("", "",sParaTemp)
     * @param apiUrl 接入机构服务接入网关URL
     * @param httpRequestMethod 请求方式：GET or POST
     * @param sPara 请求参数数组
     * @return 处理结果
     */
    public static byte[] buildRequestByByte(String apiUrl, String httpRequestMethod, Map<String, String> sPara) {
    	byte[] strResult = null;
    	HttpResponse response = getResponse(apiUrl, httpRequestMethod, sPara, HttpResultType.BYTES);
        if (response != null) {
        	try {
        		strResult = response.getByteResult();
			} catch (Exception ex) {
				LOGGER.error("调用远程HTTP接口异常：" + ex);
			}
        }
        return strResult;
    }
    
    /**
     * 
    * @Title: getResponse 
    * @Description: 建立请求，以模拟远程HTTP的构造并获取处理结果
     * @param requestUrl 接入机构服务接入网关URL
     * @param httpRequestMethod 请求方式：GET or POST
     * @param sPara 请求参数数组
    * @param resultType 类型
    * @return HttpResponse
     */
    private static HttpResponse getResponse(String requestUrl, String httpRequestMethod,Map<String, String> sPara, HttpResultType resultType){
        HttpProtocolHandler httpProtocolHandler = HttpProtocolHandler.getInstance();
        HttpRequest request = new HttpRequest(resultType);
        //设置编码集
        request.setCharset(UTF8);
        //设置请求方式
        request.setMethod(httpRequestMethod);
        request.setParameters(generatNameValuePair(sPara));
        request.setUrl(requestUrl);
        if(HTTP_METHOD_GET.equalsIgnoreCase(httpRequestMethod)){
        	request.setUrl(request.getUrl() + "?" + StringUtil.mapToUrl(sPara, false));
        }
        try{
        	HttpResponse response = httpProtocolHandler.execute(request,"","");
        	return response;
        }catch(Exception ex){
        	LOGGER.error("调用远程HTTP接口异常：" + ex);
        	return null;
        }
    }

    /**
     * MAP类型数组转换成NameValuePair类型
     * @param properties  MAP类型数组
     * @return NameValuePair类型数组
     */
    private static NameValuePair[] generatNameValuePair(Map<String, String> properties) {
        NameValuePair[] nameValuePair = new NameValuePair[properties.size()];
        int i = 0;
        for (Map.Entry<String, String> entry : properties.entrySet()) {
            nameValuePair[i++] = new NameValuePair(entry.getKey(), entry.getValue());
        }
        return nameValuePair;
    }
    
    /**
     * 
    * @Title: jsonParseObject 
    * @Description: Json转对象
    * @param text
    * @param clazz
    * @return T    返回类型 
    * @throws
     */
    protected static final <T> T jsonParseObject(String text, Class<T> clazz) {
    	T t = null;
    	try{
    		t = JSON.parseObject(text, clazz);
    	}catch(Exception ex){
    		LOGGER.error("Json转对象异常：" + ex);
    		try{
    			t = clazz.newInstance();
    		}catch(Exception e){
    			LOGGER.error("Json转对象异常：" + e);
    		}
    	}
    	return t;
    }
    
}
