package com.babe.auth;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.babe.auth.*.mapper") //扫描接口包
public class BabeShiroApplication {
    public static void main(String[] args) {
        SpringApplication.run(BabeShiroApplication.class, args);
    }

}
