package com.babe.auth.others;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Description 其它页面控制类
 * Datetime: 2020/2/18 12:24
 * Author:jason
 */
@Controller("othersView")
@RequestMapping("babe/views/" + "others")
public class ViewController {

    /**
     * 图标
     * @return
     */
    @GetMapping("babe/icon")
    @RequiresPermissions("babe:icons:view")
    public String babeIcon() {
        return  "babe/views/others/babe/icon";
    }


    @GetMapping("babe/form")
    @RequiresPermissions("babe:form:view")
    public String babeForm() {
        return  "babe/views/others/babe/form";
    }

    @GetMapping("babe/form/group")
    @RequiresPermissions("babe:formgroup:view")
    public String babeFormGroup() {
        return  "babe/views/others/babe/formGroup";
    }

    @GetMapping("babe/tools")
    @RequiresPermissions("babe:tools:view")
    public String babeTools() {
        return  "babe/views/others/babe/tools";
    }
    

    @GetMapping("babe/others")
    @RequiresPermissions("others:babe:others")
    public String babeOthers() {
        return  "babe/views/others/babe/others";
    }

    @GetMapping("apex/line")
    @RequiresPermissions("apex:line:view")
    public String apexLine() {
        return  "babe/views/others/apex/line";
    }

    @GetMapping("apex/area")
    @RequiresPermissions("apex:area:view")
    public String apexArea() {
        return  "babe/views/others/apex/area";
    }

    @GetMapping("apex/column")
    @RequiresPermissions("apex:column:view")
    public String apexColumn() {
        return  "babe/views/others/apex/column";
    }

    @GetMapping("apex/radar")
    @RequiresPermissions("apex:radar:view")
    public String apexRadar() {
        return  "babe/views/others/apex/radar";
    }

    @GetMapping("apex/bar")
    @RequiresPermissions("apex:bar:view")
    public String apexBar() {
        return  "babe/views/others/apex/bar";
    }

    @GetMapping("apex/mix")
    @RequiresPermissions("apex:mix:view")
    public String apexMix() {
        return  "babe/views/others/apex/mix";
    }

    @GetMapping("map")
    @RequiresPermissions("map:view")
    public String map() {
        return  "babe/views/others/map/gaodeMap";
    }

    @GetMapping("eximport")
    @RequiresPermissions("others:eximport:view")
    public String eximport() {
        return  "babe/views/others/eximport/eximport";
    }

    @GetMapping("eximport/result")
    public String eximportResult() {
        return  "babe/views/others/eximport/eximportResult";
    }

}
