package com.babe.auth.system.entity.po;

import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * Description
 * Datetime: 2020/2/13 11:27
 * Author:jason
 */
@TableName("sys_user_role")
public class UserRole implements Serializable {

    private static final long serialVersionUID = -1181733036601194629L;
    private Long userId; //用户ID
    private Long roleId; //角色ID

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }
}
