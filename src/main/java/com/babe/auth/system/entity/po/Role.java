package com.babe.auth.system.entity.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * Description
 * Datetime: 2020/2/13 11:26
 * Author:jason
 */
@TableName("sys_role")
public class Role implements Serializable {

    private static final long serialVersionUID = 4900005689090654329L;

    @TableId(value = "role_id", type = IdType.AUTO)
    private Long roleId; //角色ID
    private String roleName; //角色名称
    private String remark; //角色描述
    private Date createTime; //创建时间
    private Date modifyTime; //修改时间


    /**
     * 角色对应的菜单（按钮） id
     */
    private transient String menuIds;

    public String getMenuIds() {
        return menuIds;
    }

    public void setMenuIds(String menuIds) {
        this.menuIds = menuIds;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
}
