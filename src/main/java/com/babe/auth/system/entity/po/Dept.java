package com.babe.auth.system.entity.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * Description 部门实体
 * Datetime: 2020/2/13 11:26
 * Author:jason
 */
@TableName("sys_dept")
public class Dept implements Serializable {
    private static final long serialVersionUID = -5723473055495767978L;

    @TableId(value = "dept_id", type = IdType.AUTO)
    private Long deptId; //部门ID
    private Long parentId; //上级部门ID
    private String deptName; //部门名称
    private Integer orderNum; //排序
    private Date createTime; //创建时间
    private Date modifyTime; //修改时间

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Integer getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
}
