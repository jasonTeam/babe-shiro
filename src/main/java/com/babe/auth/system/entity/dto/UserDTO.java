package com.babe.auth.system.entity.dto;

import com.babe.auth.system.entity.po.User;

/**
 * Description
 * Datetime: 2020/2/14 17:17
 * Author:jason
 */
public class UserDTO extends User {

    private String roleId;
    private String roleName;
    private String deptName;

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }
}
