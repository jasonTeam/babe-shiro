package com.babe.auth.system.entity.po;

import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * Description
 * Datetime: 2020/2/13 11:26
 * Author:jason
 */
@TableName("sys_role_menu")
public class RoleMenu implements Serializable {

    private static final long serialVersionUID = -7447963661876282518L;
    private Long roleId; //角色ID
    private Long menuId; //菜单/按钮ID

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }
}
