package com.babe.auth.system.entity.dto;

/**
 * Description
 * Datetime: 2020/2/13 14:14
 * Author:jason
 */
public class LoginDTO {

    private String loginName;
    private String loginPwd;

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getLoginPwd() {
        return loginPwd;
    }

    public void setLoginPwd(String loginPwd) {
        this.loginPwd = loginPwd;
    }
}
