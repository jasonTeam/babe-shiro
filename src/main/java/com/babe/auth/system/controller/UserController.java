package com.babe.auth.system.controller;

import com.babe.auth.common.base.BaseController;
import com.babe.auth.common.exception.BabeException;
import com.babe.auth.common.results.Result;
import com.babe.auth.common.utils.MD5Util;
import com.babe.auth.common.utils.QueryRequest;
import com.babe.auth.system.Service.UserService;
import com.babe.auth.system.entity.dto.UserDTO;
import com.babe.auth.system.entity.po.User;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Map;

/**
 * Description 用户控制类
 * Datetime: 2020/2/14 8:51
 * Author:jason
 */
@Slf4j
@RestController
@RequestMapping("user")
public class UserController extends BaseController {

    @Autowired
    private UserService userService;

    /**
     * 根据用户名获取用户实体
     * @param username
     * @return
     */
    @GetMapping("{username}")
    public User getUser(@NotBlank(message = "{required}") @PathVariable String username) {
        return userService.getUserByUserName(username);
    }

    /**
     * 校验用户
     * @param username
     * @param userId
     * @return
     */
    @GetMapping("check/{username}")
    public boolean checkUserName(@NotBlank(message = "{required}") @PathVariable String username, String userId) {
        return this.userService.getUserByUserName(username) == null || StringUtils.isNotBlank(userId);
    }

    /**
     * 新增用户
     * @param
     * @return
     */
    @PostMapping("saveUser")
    @RequiresPermissions("user:add")
    public Result addUser(UserDTO userDTO) {
        log.info("请求新增用户开始。");
        this.userService.saveUser(userDTO);
        return Result.success();
    }

    /**
     * 修改用户
     * @param userDTO
     * @return
     */
    @PostMapping("update")
    @RequiresPermissions("user:update")
    public Result updateUser(UserDTO userDTO) {
        if (userDTO.getUserId() == null){
            throw new BabeException("用户ID为空");
        }
        this.userService.updateUser(userDTO);
        return Result.success();
    }


    /**
     * 分页查询用户列表
     */
    @GetMapping("list")
    @RequiresPermissions("user:view")
    public Result userList(User user,QueryRequest request) {
        log.info("请求分页查询用户列表开始");
        IPage<UserDTO> pageUserData = userService.pageListUser(user,request);
        Map<String, Object> userPageData = getDataTable(pageUserData);
        return Result.success(userPageData);
    }

    /**
     * 修改密码
     * @param oldPassword
     * @param newPassword
     * @return
     */
    @PostMapping("password/update")
    public Result updatePwd(@NotBlank(message = "{required}") String oldPassword,
                            @NotBlank(message = "{required}") String newPassword){
        log.info("请求修改密码接口");

        User user = getCurrentUser();
        if (!StringUtils.equals(user.getPassword(), MD5Util.encrypt(user.getUsername(), oldPassword))) {
            throw new BabeException("原密码不正确");
        }
        userService.updatePwd(user,newPassword);
        return  Result.success();
    }


    /**
     * 重置密码
     * @param usernames
     * @return
     */
    @PostMapping("password/reset/{usernames}")
    @RequiresPermissions("user:password:reset")
    public Result resetPassword(@NotBlank(message = "{required}") @PathVariable String usernames) {
        log.info("请求重置密码开始");
        String[] usernameArr = usernames.split(StringPool.COMMA);
        userService.resetPassword(usernameArr);
        return Result.success();
    }

    /**
     * 删除用户
     * @param userIds
     * @return
     */
    @GetMapping("delete/{userIds}")
    @RequiresPermissions("user:delete")
    public Result deleteUsers(@NotBlank(message = "{required}") @PathVariable String userIds) {
        log.info("请求删除用户接口");
        String[] uids = userIds.split(StringPool.COMMA);
        userService.deleteUsers(uids);
        return Result.success();
    }

    /**
     * 修改头像
     * @param image
     * @return
     */
    @GetMapping("avatar/{image}")
    public Result updateAvatar(@NotBlank(message = "{required}") @PathVariable String image) {
        User user = getCurrentUser();
        this.userService.updateAvatar(user.getUsername(), image);
        return Result.success();
    }



    /**
     * excel导出
     * @param queryRequest
     * @param user
     * @param response
     */
//    @GetMapping("excel")
//    @RequiresPermissions("user:export")
//    public void export(QueryRequest queryRequest, User user, HttpServletResponse response) {
//        List<UserDTO> users = this.userService.pageListUser(user, queryRequest).getRecords();
//        ExcelKit.$Export(User.class, response).downXlsx(users, false);
//    }



}
