package com.babe.auth.system.controller;

import com.babe.auth.common.base.BaseController;
import com.babe.auth.common.exception.BabeException;
import com.babe.auth.common.results.Result;
import com.babe.auth.common.tree.MenuTree;
import com.babe.auth.system.Service.MenuService;
import com.babe.auth.system.entity.po.Menu;
import com.babe.auth.system.entity.po.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

/**
 * Description
 * Datetime: 2020/2/13 21:22
 * Author:jason
 */
@Slf4j
@RestController
@RequestMapping("menu")
public class MenuController extends BaseController {

    @Autowired
    private MenuService menuService;

    /**
     * 菜单树列表
     * @return
     */
    @GetMapping("{username}") //这样获取值需要导入一个json工具包
    public Result menuTree(@NotBlank(message = "{required}") @PathVariable String username){
        User currentUser = getCurrentUser();
        if(!StringUtils.equalsIgnoreCase(username,currentUser.getUsername())){
            throw new BabeException("您无权获取别人的菜单!");
        }
        MenuTree<Menu> menuList = menuService.listMenuByUserName(username);
        return  Result.success(menuList);
    }

    /**
     * 菜单树
     * @param menu
     * @return
     */
    @GetMapping("tree")
    public Result getMenuTree(Menu menu) {
        MenuTree<Menu> menus = this.menuService.findMenus(menu);
        return Result.success(menus.getChilds());
    }

    /**
     * 新增菜单/按钮
     * @param menu
     * @return
     */
    @PostMapping("saveMenu")
    @RequiresPermissions("menu:add")
    public Result saveMenu(@Valid Menu menu) {
        this.menuService.saveMenu(menu);
        return Result.success();
    }

    /**
     * 修改菜单/按钮
     * @param menu
     * @return
     */
    @PostMapping("update")
    public Result updateMenu(@Valid Menu menu) {
        this.menuService.updateMenu(menu);
        return Result.success();
    }

    /**
     * 删除菜单/按钮
     * @param menuIds
     * @return
     */
    @GetMapping("delete/{menuIds}")
    @RequiresPermissions("menu:delete")
    public Result deleteMenus(@NotBlank(message = "{required}") @PathVariable String menuIds) {
        this.menuService.deleteMeuns(menuIds);
        return Result.success();
    }



    /**
     * 导出Excel
     * @param menu
     * @param response
     */
//    @GetMapping("excel")
//    @RequiresPermissions("menu:export")
//    public void export(Menu menu, HttpServletResponse response) {
//        List<Menu> menus = this.menuService.findMenuList(menu);
//        ExcelKit.$Export(Menu.class, response).downXlsx(menus, false);
//    }




}
