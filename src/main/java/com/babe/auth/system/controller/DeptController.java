package com.babe.auth.system.controller;

import com.babe.auth.common.results.Result;
import com.babe.auth.common.tree.DeptTree;
import com.babe.auth.system.Service.DeptService;
import com.babe.auth.system.entity.po.Dept;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.*;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.List;

/**
 * Description
 * Datetime: 2020/2/14 12:04
 * Author:jason
 */
@Slf4j
@RestController
@RequestMapping("dept")
public class DeptController {

    @Autowired
    private DeptService deptService;

    /**
     * 部门菜单树
     * @return
     */
    @GetMapping("select/tree")
    public List<DeptTree<Dept>> deptTree(){
        log.info("加载部门树菜单开始");
        return deptService.listDepts();
    }

    /**
     * 部门树
     * @param dept
     * @return
     */
    @GetMapping("tree")
    public Result getDeptTree(Dept dept){
        log.info("请求部门树");
        List<DeptTree<Dept>> deptsTree = this.deptService.DeptsTree(dept);
        return Result.success(deptsTree);
    }

    /**
     * 新增部门
     * @param dept
     * @return
     */
    @PostMapping("saveDept")
    @RequiresPermissions("dept:add")
    public Result addDept(@Valid Dept dept) {
        this.deptService.saveDept(dept);
        return Result.success();
    }

    /**
     * 修改部门
     * @param dept
     * @return
     */
    @PostMapping("update")
    @RequiresPermissions("dept:update")
    public Result updateDept(@Valid Dept dept){
        this.deptService.updateDept(dept);
        return Result.success();
    }

    /**
     * 删除部门
     * @param deptIds
     * @return
     */
    @GetMapping("delete/{deptIds}")
    @RequiresPermissions("dept:delete")
    public Result deleteDepts(@NotBlank(message = "{required}") @PathVariable String deptIds){
        String[] ids = deptIds.split(StringPool.COMMA);
        deptService.deleteDepts(ids);
        return Result.success();
    }

    /**
     * 导出excel
     * @param response
     */
    @GetMapping("excel")
    @RequiresPermissions("dept:export")
    public String getUser(HttpServletResponse response) throws Exception{
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("统计表");
        createTitle(workbook,sheet);

        List<Dept> rows =  deptService.listDept();

        //设置日期格式
        HSSFCellStyle style = workbook.createCellStyle();
        style.setDataFormat(HSSFDataFormat.getBuiltinFormat("m/d/yy h:mm"));

        //新增数据行，并且设置单元格数据
        int rowNum=1;
        for(Dept dept:rows){
            HSSFRow row = sheet.createRow(rowNum);
            row.createCell(0).setCellValue(dept.getDeptId());
            row.createCell(1).setCellValue(dept.getDeptName());
            row.createCell(2).setCellValue(dept.getOrderNum());
            HSSFCell cell = row.createCell(3);
            cell.setCellValue(dept.getCreateTime());
            cell.setCellStyle(style);
            rowNum++;
        }

        String fileName = "导出excel例子.xls";

        //生成excel文件
        buildExcelFile(fileName, workbook);

        //浏览器下载excel
        buildExcelDocument(fileName,workbook,response);

        return "download excel";
    }


    //创建表头
    private void createTitle(HSSFWorkbook workbook,HSSFSheet sheet){
        HSSFRow row = sheet.createRow(0);
        //设置列宽，setColumnWidth的第二个参数要乘以256，这个参数的单位是1/256个字符宽度
        sheet.setColumnWidth(1,12*256);
        sheet.setColumnWidth(3,17*256);

        //设置为居中加粗
        HSSFCellStyle style = workbook.createCellStyle();
        HSSFFont font = workbook.createFont();
        font.setBold(true);
        //style.setAlignment(HSSFCellStyle);
        style.setFont(font);

        HSSFCell cell;
        cell = row.createCell(0);
        cell.setCellValue("ID");
        cell.setCellStyle(style);


        cell = row.createCell(1);
        cell.setCellValue("显示名");
        cell.setCellStyle(style);

        cell = row.createCell(2);
        cell.setCellValue("用户名");
        cell.setCellStyle(style);

        cell = row.createCell(3);
        cell.setCellValue("创建时间");
        cell.setCellStyle(style);
    }

    //生成excel文件
    protected void buildExcelFile(String filename,HSSFWorkbook workbook) throws Exception{
        FileOutputStream fos = new FileOutputStream(filename);
        workbook.write(fos);
        fos.flush();
        fos.close();
    }

    //浏览器下载excel
    protected void buildExcelDocument(String filename,HSSFWorkbook workbook,HttpServletResponse response) throws Exception{
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename="+URLEncoder.encode(filename, "utf-8"));
        OutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        outputStream.flush();
        outputStream.close();
    }



}
