package com.babe.auth.system.controller;

import com.babe.auth.common.base.BaseController;
import com.babe.auth.common.exception.BabeException;
import com.babe.auth.common.results.Result;
import com.babe.auth.common.results.ResultCode;
import com.babe.auth.common.utils.MD5Util;
import com.babe.auth.common.utils.ValidateCodeUtil;
import com.babe.auth.mail.service.MailService;
import com.babe.auth.system.Service.UserService;
import com.babe.auth.system.entity.po.User;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.NotBlank;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Description 登录控制器
 * Datetime: 2020/2/13 14:05
 * Author:jason
 */
@Slf4j
@RestController
public class LoginController extends BaseController {

    @Autowired
    private ValidateCodeUtil validateCodeUtil;
    @Autowired
    private MailService mailService;

    @Value("${login.tip.subject}")
    private String subject;
    @Value("${login.tip.content}")
    private String content;



    @ApiOperation(value="登录接口", notes="登录接口")
    @PostMapping("login")
    public Result login(@NotBlank(message = "{required}") String username,
                        @NotBlank(message = "{required}") String password,
                        @NotBlank(message = "{required}") String verifyCode,
                        boolean rememberMe, HttpServletRequest request){
        log.info("请求登录接口开始");
        HttpSession session = request.getSession();
        try{
            validateCodeUtil.checkCode(session.getId(), verifyCode);
            password = MD5Util.encrypt(username.toLowerCase(), password);
            log.info("记住我的值rememberMe:[{}]",rememberMe);
            UsernamePasswordToken token = new UsernamePasswordToken(username, password, rememberMe);
            super.login(token);
        }catch (BabeException be){
            log.error("运行时异常:[{}]",be.getMessage());
            return Result.failure(ResultCode.SYS_ERROR,be.getMessage());
        }catch (Exception e){
            log.error("shiro认证异常:[{}]",e.getMessage());
            return Result.failure(ResultCode.SYS_ERROR,e.getMessage());
        }

        //登录成功后发送邮件提醒
        mailService.sendSimpleMail(getCurrentUser().getEmail(),subject,content);
        return Result.success();
    }


    /**
     * 最近登录统计
     * @param username
     * @return
     */
    @GetMapping("index/{username}")
    public Result index(@NotBlank(message = "{required}") @PathVariable String username) {

        Map<String, Object> data = new HashMap<>();
        data.put("totalVisitCount", "1000");
        data.put("todayVisitCount", "200");
        data.put("todayIp", "127.0.0.1");
        // 获取近期系统访问记录
        data.put("lastSevenVisitCount", "58000");
        data.put("lastSevenUserVisitCount", "8000000");

        return Result.success(data);
    }

    /**
     * 图形验证码
     * @return
     */
    @GetMapping("images/captcha")
    public void validateCode(HttpServletRequest request, HttpServletResponse response) throws IOException {
        log.info("请求图形验证码");
        validateCodeUtil.createValidateCode(request,response);
    }






}
