package com.babe.auth.system.controller;

import com.babe.auth.common.base.BaseController;
import com.babe.auth.common.enums.SexEnum;
import com.babe.auth.common.shiro.ShiroHelper;
import com.babe.auth.common.utils.DateUtil;
import com.babe.auth.system.Service.UserService;
import com.babe.auth.system.entity.po.User;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Description: 页面专用控制类
 * Datetime: 2020/2/9 23:03
 * Author:jason
 */
@Controller
public class ViewController extends BaseController {

    private static final String VIEW_PATH_PRE = "babe/views/";

    @Autowired
    private UserService userService;
    @Autowired
    private ShiroHelper shiroHelper;

    /**
     * 登录页面
     *
     * @return
     */
    @GetMapping("login")
    public ModelAndView loginView() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName(VIEW_PATH_PRE + "login");
        return mav;
    }

    /**
     * 首页页面
     *
     * @param model
     * @return
     */
    @GetMapping("index")
    public String indexPage(Model model) {
        AuthorizationInfo authorizationInfo = shiroHelper.getCurrentuserAuthorizationInfo();
        User user = super.getCurrentUser();
        User currentUserDetail = userService.getUserByUserName(user.getUsername());
        currentUserDetail.setPassword("**************");
        model.addAttribute("user", currentUserDetail);
        model.addAttribute("permissions", authorizationInfo.getStringPermissions());
        model.addAttribute("roles", authorizationInfo.getRoles());
        return "index";
    }

    /**
     * 到首页
     *
     * @return
     */
    @RequestMapping(VIEW_PATH_PRE + "index")
    public String pageIndex() {
        return VIEW_PATH_PRE + "index";
    }

    /**
     * 重定向到首页
     *
     * @return
     */
    @GetMapping("/")
    public String redirectIndex() {
        return "redirect:/index";
    }

    /**
     * 侧边栏菜单渲染
     *
     * @return
     */
    @GetMapping(VIEW_PATH_PRE + "layout")
    public String layout() {
        return VIEW_PATH_PRE + "layout";
    }

    //***********************用户相关页面**************************

    /**
     * 用户列表
     *
     * @return
     */
    @GetMapping(VIEW_PATH_PRE + "system/user")
    @RequiresPermissions("user:view")
    public String systemUser() {
        return VIEW_PATH_PRE + "system/user/user";
    }

    /**
     * 修改密码
     *
     * @return
     */
    @GetMapping(VIEW_PATH_PRE + "password/update")
    public String passwordUpdate() {

        return VIEW_PATH_PRE + "system/user/passwordUpdate";
    }

    /**
     * 个人信息
     *
     * @return
     */
    @GetMapping(VIEW_PATH_PRE + "user/profile")
    public String userProfile() {
        return VIEW_PATH_PRE + "system/user/userProfile";
    }

    /**
     * 头像
     *
     * @return
     */
    @GetMapping(VIEW_PATH_PRE + "user/avatar")
    public String userAvatar() {
        return VIEW_PATH_PRE + "system/user/avatar";
    }

    /**
     * 修改个人信息
     *
     * @return
     */
    @GetMapping(VIEW_PATH_PRE + "user/profile/update")
    public String profileUpdate() {
        return VIEW_PATH_PRE + "system/user/profileUpdate";
    }

    /**
     * 添加用户
     *
     * @return
     */
    @GetMapping(VIEW_PATH_PRE + "system/user/add")
    @RequiresPermissions("user:add")
    public String systemUserAdd() {
        return VIEW_PATH_PRE + "system/user/userAdd";
    }

    /**
     * 用户详情弹窗页面
     *
     * @param username
     * @param model
     * @return
     */
    @GetMapping(VIEW_PATH_PRE + "system/user/detail/{username}")
    @RequiresPermissions("user:view")
    public String systemUserDetail(@PathVariable String username, Model model) {
        User user = userService.getUserByUserName(username);
        model.addAttribute("user", user);
        user.setSex(SexEnum.getEnumByValue(user.getSex()).getDesc()); //用户性别使用枚举转为中文
        if (user.getLastLoginTime() != null) {
            model.addAttribute("lastLoginTime", user.getLastLoginTime());
        }
        return VIEW_PATH_PRE + "system/user/userDetail";
    }

    /**
     * 用户修改弹窗页面
     *
     * @param username
     * @param model
     * @return
     */
    @GetMapping(VIEW_PATH_PRE + "system/user/update/{username}")
    @RequiresPermissions("user:update")
    public String systemUserUpdate(@PathVariable String username, Model model) {
        User user = userService.getUserByUserName(username);
        model.addAttribute("user", user);
        if (user.getLastLoginTime() != null) {
            model.addAttribute("lastLoginTime", user.getLastLoginTime());
        }
        return VIEW_PATH_PRE + "system/user/userUpdate";
    }

    /**
     * 角色管理页面
     *
     * @return
     */
    @GetMapping(VIEW_PATH_PRE + "system/role")
    @RequiresPermissions("role:view")
    public String systemRole() {
        return VIEW_PATH_PRE + "system/role/role";
    }

    /**
     * 菜单管理页面
     *
     * @return
     */
    @GetMapping(VIEW_PATH_PRE + "system/menu")
    @RequiresPermissions("menu:view")
    public String systemMenu() {
        return VIEW_PATH_PRE + "system/menu/menu";
    }

    /**
     * 部门管理页面
     *
     * @return
     */
    @GetMapping(VIEW_PATH_PRE + "system/dept")
    @RequiresPermissions("dept:view")
    public String systemDept() {
        return VIEW_PATH_PRE + "system/dept/dept";
    }


    /**
     * 错误页面404
     *
     * @return
     */
    @GetMapping(VIEW_PATH_PRE + "404")
    public String error404() {
        return VIEW_PATH_PRE + "error/404";
    }

    /**
     * 错误页面403
     *
     * @return
     */
    @GetMapping(VIEW_PATH_PRE + "403")
    public String error403() {
        return VIEW_PATH_PRE + "error/403";
    }

    /**
     * 错误页面500
     *
     * @return
     */
    @GetMapping(VIEW_PATH_PRE + "500")
    public String error500() {
        return VIEW_PATH_PRE + "error/500";
    }

    /**
     * 未认证跳转页面
     *
     * @return
     */
    @GetMapping("unauthorized")
    public String unauthorized() {
        return VIEW_PATH_PRE + "error/500";
    }


}
