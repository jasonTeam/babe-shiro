package com.babe.auth.system.controller;

import com.babe.auth.common.base.BaseController;
import com.babe.auth.common.results.Result;
import com.babe.auth.common.utils.QueryRequest;
import com.babe.auth.system.Service.RoleService;
import com.babe.auth.system.entity.po.Role;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Map;

/**
 * Description
 * Datetime: 2020/2/13 21:27
 * Author:jason
 */
@Slf4j
@RestController
@RequestMapping("role")
public class RoleController extends BaseController {

    @Autowired
    private RoleService roleService;

    /**
     * 获取所有角色列表
     * @param role
     * @return
     */
    @GetMapping
    public Result getAllRoles(Role role) {
        log.info("获取所有的角色列表");
        List<Role> roleList = roleService.listRole(role);
        return Result.success(roleList);
    }

    /**
     * 分页查询角色列表
     * @param role
     * @param request
     * @return
     */
    @GetMapping("list")
    @RequiresPermissions("role:view")
    public Result roleList(Role role, QueryRequest request) {
        Map<String, Object> dataTable = getDataTable(this.roleService.pageListRole(role, request));
        return Result.success(dataTable);
    }

    /**
     * 新增角色
     * @param role
     * @return
     */
    @PostMapping("saveRole")
    @RequiresPermissions("role:add")
    public Result saveRole(@Valid Role role) {
        log.info("新增角色");
        this.roleService.saveRole(role);
        return Result.success();
    }

    /**
     * 修改角色
     * @param role
     * @return
     */
    @PostMapping("update")
    @RequiresPermissions("role:update")
    public Result updateRole(Role role) {
        log.info("修改角色");
        this.roleService.updateRole(role);
        return new Result().success();
    }


    /**
     * 删除角色
     * @param roleIds
     * @return
     */
    @GetMapping("delete/{roleIds}")
    @RequiresPermissions("role:delete")
    public Result deleteRoles(@NotBlank(message = "{required}") @PathVariable String roleIds) {
        this.roleService.deleteRoles(roleIds);
        return Result.success();
    }


//
//    /**
//     * 导出excel
//     * @param queryRequest
//     * @param role
//     * @param response
//     * @throws FebsException
//     */
//    @GetMapping("excel")
//    @RequiresPermissions("role:export")
//    public void export(QueryRequest queryRequest, Role role, HttpServletResponse response) throws FebsException {
//        List<Role> roles = this.roleService.findRoles(role, queryRequest).getRecords();
//        ExcelKit.$Export(Role.class, response).downXlsx(roles, false);
//    }



}
