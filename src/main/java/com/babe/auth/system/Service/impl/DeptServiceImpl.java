package com.babe.auth.system.Service.impl;

import com.babe.auth.common.tree.DeptTree;
import com.babe.auth.common.tree.MenuTree;
import com.babe.auth.common.tree.TreeUtil;
import com.babe.auth.common.utils.QueryRequest;
import com.babe.auth.common.utils.SortUtil;
import com.babe.auth.system.Service.DeptService;
import com.babe.auth.system.entity.po.Dept;
import com.babe.auth.system.entity.po.Menu;
import com.babe.auth.system.mapper.DeptMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Description
 * Datetime: 2020/2/14 12:09
 * Author:jason
 */
@Service("deptService")
public class DeptServiceImpl extends ServiceImpl<DeptMapper,Dept> implements DeptService {


    @Override
    public List<DeptTree<Dept>> listDepts() {
        List<Dept> deptList = this.baseMapper.listDept();
        List<DeptTree<Dept>> trees = this.convertDeptsList(deptList);
        return TreeUtil.buildDeptTree(trees);
    }

    @Override
    public List<Dept> listDept() {
        return this.baseMapper.listDept();
    }

    /**
     * 部门树
     * @param dept
     * @return
     */
    @Override
    public List<DeptTree<Dept>> DeptsTree(Dept dept) {
        QueryWrapper<Dept> queryWrapper = new QueryWrapper<>();

        if (StringUtils.isNotBlank(dept.getDeptName())){
            queryWrapper.lambda().eq(Dept::getDeptName, dept.getDeptName());
        }
        queryWrapper.lambda().orderByAsc(Dept::getOrderNum);

        List<Dept> depts = this.baseMapper.selectList(queryWrapper);
        List<DeptTree<Dept>> trees =  this.convertDeptsList(depts);
        return TreeUtil.buildDeptTree(trees);
    }

    /**
     * 新增部门
     * @param dept
     */
    @Override
    public void saveDept(Dept dept) {
        Long parentId = dept.getParentId();
        if (parentId == null){
            dept.setParentId(0L);
        }
        dept.setCreateTime(new Date());
        this.save(dept);
    }

    /**
     * 修改部门
     * @param dept
     */
    @Transactional
    @Override
    public void updateDept(Dept dept) {
        dept.setDeptName(dept.getDeptName());
        dept.setOrderNum(dept.getOrderNum());
        dept.setModifyTime(new Date());
        this.baseMapper.updateDept(dept);
    }

    /**
     * 删除部门
     * @param deptIds
     */
    @Transactional
    @Override
    public void deleteDepts(String[] deptIds) {
        for(String deptId : deptIds){
            this.baseMapper.delUserByDeptId(deptId);
        }
    }

    /**
     * 部门列表
     * @param dept
     * @param request
     * @return
     */
    @Override
    public List<Dept> listDepts(Dept dept, QueryRequest request) {
        QueryWrapper<Dept> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(dept.getDeptName())){
            queryWrapper.lambda().eq(Dept::getDeptName, dept.getDeptName());
        }
        SortUtil.handleWrapperSort(request, queryWrapper, "orderNum", "asc", true);
        return this.baseMapper.selectList(queryWrapper);
    }

    /**
     * 部门列表转换
     * @param depts
     * @return
     */
    private List<DeptTree<Dept>> convertDeptsList(List<Dept> depts){
        List<DeptTree<Dept>> trees = new ArrayList<>();
        depts.forEach(dept -> {
            DeptTree<Dept> tree = new DeptTree<>();
            tree.setId(String.valueOf(dept.getDeptId()));
            tree.setParentId(String.valueOf(dept.getParentId()));
            tree.setName(dept.getDeptName());
            tree.setData(dept);
            trees.add(tree);
        });
        return trees;
    }




}
