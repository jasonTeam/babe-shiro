package com.babe.auth.system.Service.impl;

import com.babe.auth.common.shiro.MyRealm;
import com.babe.auth.common.tree.MenuTree;
import com.babe.auth.common.tree.TreeUtil;
import com.babe.auth.system.Service.MenuService;
import com.babe.auth.system.Service.RoleMenuService;
import com.babe.auth.system.entity.po.Menu;
import com.babe.auth.system.mapper.MenuMapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Description
 * Datetime: 2020/2/13 20:21
 * Author:jason
 */
@Service("menuService")
public class MenuServiceImpl extends ServiceImpl<MenuMapper,Menu> implements MenuService {

    @Autowired
    private MyRealm myRealm;
    @Autowired
    private RoleMenuService roleMenuService;


    /**
     * 通过用户名获取菜单权限列表
     * @param userName
     * @return
     */
    @Override
    public List<String> listMenuPermsByUserName(String userName) {
        return this.baseMapper.listMenuPermsByUserName(userName);
    }

    /**
     * 通过当前登录的用户的用户名获取其菜单集合
     * @param userName
     * @return
     */
    @Override
    public  MenuTree<Menu> listMenuByUserName(String userName) {
        List<Menu> menusList = this.baseMapper.listMenuByUserName(userName);
        List<MenuTree<Menu>> trees = this.convertListMenus(menusList);
        return TreeUtil.buildMenuTree(trees);
    }

    /**
     * 获取菜单树
     * @param menu
     * @return
     */
    @Override
    public MenuTree<Menu> findMenus(Menu menu) {
        QueryWrapper<Menu> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(menu.getMenuName())) {
            queryWrapper.lambda().like(Menu::getMenuName, menu.getMenuName());
        }
        queryWrapper.lambda().orderByAsc(Menu::getOrderNum);
        List<Menu> menus = this.baseMapper.selectList(queryWrapper);
        List<MenuTree<Menu>> trees = this.convertListMenus(menus);

        return TreeUtil.buildMenuTree(trees);
    }

    @Transactional
    @Override
    public void saveMenu(Menu menu) {
        menu.setCreateTime(new Date());
        this.setMenu(menu);
        this.baseMapper.insert(menu);
    }

    @Transactional
    @Override
    public void updateMenu(Menu menu) {
        menu.setModifyTime(new Date());
        this.setMenu(menu);
        this.baseMapper.updateById(menu);

        myRealm.clearCache();
    }

    @Transactional
    @Override
    public void deleteMeuns(String menuIds) {
        String[] menuIdsArray = menuIds.split(StringPool.COMMA);
        this.delete(Arrays.asList(menuIdsArray));

        myRealm.clearCache();
    }


    /**
     * 转换菜单列表
     * @param menuList
     * @return
     */
    private List<MenuTree<Menu>> convertListMenus(List<Menu> menuList){
        List<MenuTree<Menu>> menuTrees = new ArrayList<>();
        for(Menu menu : menuList){
            MenuTree<Menu> tree = new MenuTree<>();
            tree.setId(String.valueOf(menu.getMenuId()));
            tree.setParentId(String.valueOf(menu.getParentId()));
            tree.setTitle(menu.getMenuName());
            tree.setIcon(menu.getIcon());
            tree.setHref(menu.getMenuUrl());
            tree.setData(menu);
            menuTrees.add(tree);
        }
        return menuTrees;
    }

    /**
     * 设置菜单/按钮
     * @param menu
     */
    private void setMenu(Menu menu) {
        if (menu.getParentId() == null)
            menu.setParentId(0L);
        if ("1".equals(menu.getType())) {
            menu.setMenuUrl(null);
            menu.setIcon(null);
        }
    }


    //删除
    private void delete(List<String> menuIds) {
        List<String> list = new ArrayList<>(menuIds);
        removeByIds(menuIds);

        LambdaQueryWrapper<Menu> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(Menu::getParentId, menuIds);
        List<Menu> menus = baseMapper.selectList(queryWrapper);
        if (CollectionUtils.isNotEmpty(menus)) {
            List<String> menuIdList = new ArrayList<>();
            menus.forEach(m -> menuIdList.add(String.valueOf(m.getMenuId())));
            list.addAll(menuIdList);
            this.roleMenuService.deleteRoleMenusByMenuId(list);
            this.delete(menuIdList);
        } else {
            this.roleMenuService.deleteRoleMenusByMenuId(list);
        }
    }


}
