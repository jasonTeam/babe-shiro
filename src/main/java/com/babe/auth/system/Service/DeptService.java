package com.babe.auth.system.Service;

import com.babe.auth.common.tree.DeptTree;
import com.babe.auth.common.utils.QueryRequest;
import com.babe.auth.system.entity.po.Dept;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * Description
 * Datetime: 2020/2/14 12:08
 * Author:jason
 */
public interface DeptService extends IService<Dept>{

    /**
     * 部门列表
     * @return
     */
    List<DeptTree<Dept>> listDepts();

    /**
     *  部门列表集合
     * @return
     */
    List<Dept> listDept();

    /**
     * 部门树
     * @param dept
     * @return
     */
    List<DeptTree<Dept>> DeptsTree(Dept dept);

    /**
     * 新增部门
     * @param dept
     */
    void saveDept(Dept dept);

    /**
     * 修改部门
     * @param dept
     */
    void updateDept(Dept dept);

    /**
     * 删除部门
     * @param deptIds
     */
    void deleteDepts(String[] deptIds);

    /**
     * 部门列表
     * @param dept
     * @param request
     * @return
     */
    List<Dept> listDepts(Dept dept, QueryRequest request);
}
