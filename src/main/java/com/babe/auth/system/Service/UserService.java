package com.babe.auth.system.Service;

import com.babe.auth.common.utils.QueryRequest;
import com.babe.auth.system.entity.dto.UserDTO;
import com.babe.auth.system.entity.po.User;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * Description
 * Datetime: 2020/2/13 15:44
 * Author:jason
 */
public interface UserService extends IService<User> {

    /**
     * 根据用户名获取用户
     * @param userName
     * @return
     */
    User getUserByUserName(String userName);

    /**
     * 新增用户
     * @param
     */
    void saveUser(UserDTO userDTO);

    /**
     * 修改用户
     * @param userDTO
     */
    void updateUser(UserDTO userDTO);

    /**
     * 修改密码
     * @param user
     * @param newPwd
     * @return
     */
    void updatePwd(User user,String newPwd);

    /**
     * 重置密码
     * @param username
     */
    void resetPassword(String[] username);

    /**
     * 删除用户
     * @param userIds
     */
    void deleteUsers(String[] userIds);


    /**
     * 分页查询用户列表
     * @param user
     * @return
     */
    IPage<UserDTO> pageListUser(User user, QueryRequest request);

    /**
     * 更新头像
     * @param username
     * @param avatar
     */
    void updateAvatar(String username, String avatar);

    void insertUser();


}
