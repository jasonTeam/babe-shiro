package com.babe.auth.system.Service;

import com.babe.auth.common.tree.MenuTree;
import com.babe.auth.system.entity.po.Menu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * Description
 * Datetime: 2020/2/13 20:19
 * Author:jason
 */
public interface MenuService extends IService<Menu> {

    /**
     * 通过用户名获取菜单权限按钮列表
     * @param userName
     * @return
     */
    List<String> listMenuPermsByUserName(String userName);

    /**
     * 通过当前登录的用户的用户名获取其菜单集合
     * @param userName
     * @return
     */
    MenuTree<Menu> listMenuByUserName(String userName);

    /**
     * 获取菜单树
     * @param menu
     * @return
     */
    MenuTree<Menu> findMenus(Menu menu);

    /**
     * 新增菜单/按钮
     * @param menu
     */
    void saveMenu(Menu menu);

    /**
     * 修改菜单/按钮
     * @param menu
     */
    void updateMenu(Menu menu);

    /**
     * 删除菜单/按钮
     * @param menuIds
     */
    void deleteMeuns(String menuIds);

}
