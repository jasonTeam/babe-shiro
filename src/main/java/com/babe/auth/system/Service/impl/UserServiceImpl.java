package com.babe.auth.system.Service.impl;

import com.babe.auth.common.enums.UserEnum;
import com.babe.auth.common.shiro.MyRealm;
import com.babe.auth.common.utils.DateUtil;
import com.babe.auth.common.utils.MD5Util;
import com.babe.auth.common.utils.QueryRequest;
import com.babe.auth.system.Service.UserRoleService;
import com.babe.auth.system.Service.UserService;
import com.babe.auth.system.entity.dto.UserDTO;
import com.babe.auth.system.entity.po.User;
import com.babe.auth.system.entity.po.UserRole;
import com.babe.auth.system.mapper.UserMapper;
import com.babe.auth.system.mapper.UserRoleMapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Description
 * Datetime: 2020/2/13 15:46
 * Author:jason
 */
@Service("userService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class UserServiceImpl extends ServiceImpl<UserMapper,User> implements UserService {

    @Resource
    private UserMapper userMapper;
    @Resource
    private UserRoleMapper userRoleMapper;
    @Autowired
    private UserRoleService userRoleService;
    @Autowired
    private MyRealm myRealm;

    /**
     * 根据用户名获取用户
     * @param userName
     * @return
     */
    @Override
    public User getUserByUserName(String userName) {
        User user = this.baseMapper.getUserByUserName(userName);
        return user;
    }

    /**
     * 新增用户
     * @param
     */
    @Transactional
    @Override
    public void saveUser(UserDTO userDTO) {
        User user = new User();
        user.setStatus(UserEnum.USER_STATUS_NORMAL.value()); //用户状态
        user.setSex(userDTO.getSex()); //性别
        user.setEmail(userDTO.getEmail()); //邮箱
        user.setMobile(userDTO.getMobile()); //联系方式
        user.setAvatar(UserEnum.DEF_AVATAR.value()); //默认头像
        user.setTheme(UserEnum.DEF_THEME.value()); //主题
        user.setIsTab(UserEnum.IS_TAB.value()); //
        user.setDeptId(userDTO.getDeptId()); //部门ID
        user.setUsername(userDTO.getUsername()); //用户名
        user.setPassword(MD5Util.encrypt(userDTO.getUsername(),UserEnum.DEF_PWD.value())); //默认密码
        user.setCreateTime(DateUtil.getCurrentDateTime()); //创建时间
        user.setDescription(userDTO.getDescription()); //备注
        save(user);
        // 保存用户角色
        String[] roles = userDTO.getRoleId().split(StringPool.COMMA);
        setUserRoles(user,roles);
    }

    /**
     * 修改用户
     * @param userDTO
     */
    @Override
    @Transactional
    public void updateUser(UserDTO userDTO) {
        String username = userDTO.getUsername();
        // 更新用户
        userDTO.setPassword(null);
        userDTO.setUsername(null);
        userDTO.setModifyTime(DateUtil.getCurrentDateTime());
        updateById(userDTO);
        // 更新关联角色
        this.userRoleService.remove(new LambdaQueryWrapper<UserRole>().eq(UserRole::getUserId, userDTO.getUserId()));
        String[] roles = userDTO.getRoleId().split(StringPool.COMMA);
        setUserRoles(userDTO, roles);

        User currentUser = (User) SecurityUtils.getSubject().getPrincipal();
        if (StringUtils.equalsIgnoreCase(currentUser.getUsername(), username)) {
            myRealm.clearCache();
        }
    }

    //保存角色
    private void setUserRoles(User user,String[] roles) {
        List<UserRole> userRoles = new ArrayList<>();
        Arrays.stream(roles).forEach(roleId -> {
            UserRole userRole = new UserRole();
            userRole.setUserId(user.getUserId());
            userRole.setRoleId(Long.valueOf(roleId));
            userRoles.add(userRole);
        });
        userRoleService.saveBatch(userRoles);
    }


    /**
     * 修改密码
     * @param user
     * @param newPwd
     * @return
     */
    @Transactional
    @Override
    public void updatePwd(User user,String newPwd) {
        newPwd = MD5Util.encrypt(user.getUsername(),newPwd);
        this.baseMapper.updatePwd(user.getUserId(),newPwd,DateUtil.getCurrentDateTime());
    }

    /**
     * 重置密码
     * @param username
     */
    @Transactional
    @Override
    public void resetPassword(String[] username) {
        for(String name : username){
            String defaultPwd = MD5Util.encrypt(name,"888888");
            this.baseMapper.resetPassword(name,defaultPwd,DateUtil.getCurrentDateTime());
        }
    }

    /**
     * 删除用户
     * @param userIds
     */
    @Transactional
    @Override
    public void deleteUsers(String[] userIds) {
        for(String userId : userIds){
            userMapper.delUserByUid(userId); //删除用户
            userRoleMapper.delUserRoleByUid(userId); //删除用户对应的角色
        }
    }

    /**
     * 分页查询用户列表
     * @param user
     * @return
     */
    @Override
    public IPage<UserDTO> pageListUser(User user, QueryRequest request) {
        Page<UserDTO> page = new Page<>(request.getPageNum(),request.getPageSize());
        IPage<UserDTO> pageData = this.baseMapper.pageListUser(page,user);
        return pageData;
    }

    /**
     * 更新头像
     * @param username
     * @param avatar
     */
    @Override
    public void updateAvatar(String username, String avatar) {
        User user = new User();
        user.setAvatar(avatar);
        user.setModifyTime(DateUtil.getCurrentDateTime());
        this.baseMapper.update(user, new LambdaQueryWrapper<User>().eq(User::getUsername, username));
    }

    @Override
    public void insertUser() {

        List<User> userList = new ArrayList<>();

        for(int i=0; i < 1000; i++){
            User user = new User();
            user.setMobile("1526715184"+i);
            user.setUsername("test"+i);
            user.setPassword("123456");
            user.setStatus("1");
            user.setEmail("23503270"+i+"@qq.com");
            user.setCreateTime(DateUtil.getCurrentDateTime());
            userList.add(user);
        }
        this.userMapper.insertUser(userList);
    }

}
