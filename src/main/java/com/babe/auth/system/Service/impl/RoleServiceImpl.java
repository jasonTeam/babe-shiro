package com.babe.auth.system.Service.impl;

import com.babe.auth.common.shiro.MyRealm;
import com.babe.auth.common.utils.QueryRequest;
import com.babe.auth.system.Service.RoleMenuService;
import com.babe.auth.system.Service.RoleService;
import com.babe.auth.system.Service.UserRoleService;
import com.babe.auth.system.entity.po.Role;
import com.babe.auth.system.entity.po.RoleMenu;
import com.babe.auth.system.mapper.RoleMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Description
 * Datetime: 2020/2/13 20:23
 * Author:jason
 */
@Service("roleService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class RoleServiceImpl extends ServiceImpl<RoleMapper,Role> implements RoleService {

    @Autowired
    private RoleMenuService roleMenuService;
    @Autowired
    private UserRoleService userRoleService;
    @Autowired
    private MyRealm myRealm;

    /**
     * 通过用户名获取角色列表
     * @param userName
     * @return
     */
    @Override
    public List<Role> listRolesByUserName(String userName) {
        return this.baseMapper.listRolesByUserName(userName);
    }

    /**
     * 角色列表
     * @return
     */
    @Override
    public List<Role> listRole(Role role) {
        QueryWrapper<Role> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(role.getRoleName())){
            queryWrapper.lambda().like(Role::getRoleName, role.getRoleName());
        }
        return this.baseMapper.selectList(queryWrapper);
    }

    /**
     * 分页查询角色列表
     * @param role
     * @param request
     * @return
     */
    @Override
    public IPage<Role> pageListRole(Role role, QueryRequest request) {
        Page<Role> page = new Page<>(request.getPageNum(), request.getPageSize());
        return this.baseMapper.pageListRole(page, role);
    }

    /**
     * 新增角色
     * @param role
     */
    @Transactional
    @Override
    public void saveRole(Role role) {
        role.setCreateTime(new Date());
        this.baseMapper.insert(role);
        this.saveRoleMenus(role);
    }

    /**
     * 修改角色
     * @param role
     */
    @Override
    @Transactional
    public void updateRole(Role role) {
        role.setModifyTime(new Date());
        this.updateById(role);
        List<String> roleIdList = new ArrayList<>();
        roleIdList.add(String.valueOf(role.getRoleId()));
        this.roleMenuService.deleteRoleMenusByRoleId(roleIdList);
        saveRoleMenus(role);

        myRealm.clearCache();
    }

    /**
     * 删除角色
     * @param roleIds
     */
    @Override
    public void deleteRoles(String roleIds) {
        List<String> list = Arrays.asList(roleIds.split(StringPool.COMMA));
        this.baseMapper.delete(new QueryWrapper<Role>().lambda().in(Role::getRoleId, list));

        this.roleMenuService.deleteRoleMenusByRoleId(list);
        this.userRoleService.deleteUserRolesByRoleId(list);
    }

    /**
     * 保存角色和菜单之间的关系
     * @param role
     */
    private void saveRoleMenus(Role role) {
        if (StringUtils.isNotBlank(role.getMenuIds())) {
            String[] menuIds = role.getMenuIds().split(StringPool.COMMA);
            List<RoleMenu> roleMenus = new ArrayList<>();
            Arrays.stream(menuIds).forEach(menuId -> {
                RoleMenu roleMenu = new RoleMenu();
                roleMenu.setMenuId(Long.valueOf(menuId));
                roleMenu.setRoleId(role.getRoleId());
                roleMenus.add(roleMenu);
            });
            roleMenuService.saveBatch(roleMenus);
        }
    }


}
