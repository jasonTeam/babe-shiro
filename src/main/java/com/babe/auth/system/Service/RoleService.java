package com.babe.auth.system.Service;

import com.babe.auth.common.utils.QueryRequest;
import com.babe.auth.system.entity.po.Role;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * Description
 * Datetime: 2020/2/13 20:19
 * Author:jason
 */
public interface RoleService extends IService<Role> {

    /**
     * 通过用户名获取角色列表
     * @param userName
     * @return
     */
    List<Role> listRolesByUserName(String userName);

    /**
     * 角色列表
     * @return
     */
    List<Role> listRole(Role role);

    /**
     * 分页查询角色列表
     * @param role
     * @param request
     * @return
     */
    IPage<Role> pageListRole(Role role,QueryRequest request);

    /**
     * 新增角色
     * @param role
     */
    void saveRole(Role role);

    /**
     * 修改角色
     * @param role
     */
    void updateRole(Role role);

    /**
     * 删除角色
     * @param roleIds
     */
    void deleteRoles(String roleIds);




}
