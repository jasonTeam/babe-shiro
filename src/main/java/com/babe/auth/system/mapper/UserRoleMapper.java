package com.babe.auth.system.mapper;

import com.babe.auth.system.entity.po.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * Description 用户角色表
 * Datetime: 2020/2/15 14:53
 * Author:jason
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

    /**
     * 根据用户ID删除userRole数据
     * @param userId
     * @return
     */
    int delUserRoleByUid(String userId);



}
