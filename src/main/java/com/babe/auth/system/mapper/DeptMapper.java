package com.babe.auth.system.mapper;

import com.babe.auth.system.entity.po.Dept;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * Description 部门接口
 * Datetime: 2020/2/14 12:07
 * Author:jason
 */
public interface DeptMapper extends BaseMapper<Dept> {

    /**
     * 部门列表
     * @return
     */
    List<Dept> listDept();

    /**
     * 修改部门
     * @param dept
     */
    void updateDept(Dept dept);

    /**
     * 删除部门
     * @param deptId
     */
    void delUserByDeptId(String deptId);

}
