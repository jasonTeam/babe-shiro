package com.babe.auth.system.mapper;

import com.babe.auth.system.entity.po.Menu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * Description
 * Datetime: 2020/2/13 20:15
 * Author:jason
 */
public interface MenuMapper extends BaseMapper<Menu> {

    /**
     * 通过用户名获取菜单权限列表
     * @param userName
     * @return
     */
    List<String> listMenuPermsByUserName(String userName);

    /**
     * 通过当前登录的用户的用户名获取其菜单集合
     * @param userName
     * @return
     */
    List<Menu> listMenuByUserName(String userName);


}
