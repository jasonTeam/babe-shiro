package com.babe.auth.system.mapper;

import com.babe.auth.system.entity.dto.UserDTO;
import com.babe.auth.system.entity.po.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Description
 * Datetime: 2020/2/13 15:25
 * Author:jason
 */
public interface UserMapper extends BaseMapper<User> {

    /**
     * 根据用户名获取用户
     * @param userName
     * @return
     */
    User getUserByUserName(String userName);

    /**
     * 修改密码
     * @param userId 用户ID
     * @param modifyTime 修改时间
     * @return
     */
    int updatePwd(@Param("userId")Long userId,
                  @Param("passwrod")String passwrod,
                  @Param("modifyTime")String modifyTime);

    /**
     * 重置密码
     * @param username
     * @param defaultPwd
     * @param modifyTime
     */
    int resetPassword(@Param("username")String username,
                       @Param("defaultPwd")String defaultPwd,
                       @Param("modifyTime")String modifyTime);


    /**
     * 分页查询用户列表
     * @param user
     * @return
     */
    IPage<UserDTO> pageListUser(Page page,@Param("user") User user);

    /**
     * 通过用户ID删除数据
     * @param userId
     * @return
     */
    int delUserByUid(String userId);

    /**
     * 循环插入测试
     * @return
     */
    int insertUser(List<User> list);


}
