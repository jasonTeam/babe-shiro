package com.babe.auth.system.mapper;

import com.babe.auth.system.entity.po.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Description
 * Datetime: 2020/2/13 20:16
 * Author:jason
 */
public interface RoleMapper extends BaseMapper<Role> {

    /**
     * 通过当前登录的用户名获取角色集合
     * @param userName
     * @return
     */
    List<Role> listRolesByUserName(String userName);

    /**
     * 角色列表
     * @return
     */
    List<Role> listRole();

    /**
     * 分页查询角色列表
     * @param page
     * @param role
     * @return
     */
    IPage<Role> pageListRole(Page page, @Param("role") Role role);




}
