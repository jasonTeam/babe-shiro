package com.babe.auth.system.mapper;

import com.babe.auth.system.entity.po.RoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * Description 角色菜单关系表
 * Datetime: 2020/2/18 10:51
 * Author:jason
 */
public interface RoleMenuMapper extends BaseMapper<RoleMenu> {
}
