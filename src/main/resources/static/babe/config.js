// 定义模块 方法：layui.define([mods], callback)
console.log("加载config.js")
layui.define(function(exports) {
  exports('conf', {
    container: 'babe',
    containerBody: 'babe-body',
    v: '2.0',
    base: layui.cache.base,
    css: layui.cache.base + 'css/',
    views: layui.cache.base + 'views/',
    viewLoadBar: true,
    debug: layui.cache.debug,
    name: 'babe',
    entry: '/index',
    engine: '',
    eventName: 'babe-event',
    tableName: 'babe',
    requestUrl: './'
  })
});
