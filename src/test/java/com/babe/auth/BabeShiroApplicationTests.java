package com.babe.auth;

import com.babe.auth.common.mq.sender.Sender;
import com.babe.auth.mail.service.MailService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.mail.MessagingException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BabeShiroApplicationTests {

//    @Autowired
//    MailService mailService;

    @Autowired
    private Sender sender;

//    @Test //测试邮件发送
//    public void test() throws MessagingException {
//        mailService.sendSimpleMail("shenziyang1991@qq.com", "这是我的一个测试", "呵呵呵呵呵呵呵呵");
//    }

//    @Test //测试邮件发送
//    public void test() throws MessagingException {
//        String content="<html>\n"+
//                "<body>\n"+
//                "<h3>hello world</h3>\n"+
//                "</body>\n"+
//                "</html>";
//        mailService.sendHtmlMail("shenziyang1991@qq.com","主题",content);
//    }

    //测试rabbitmq发送单条消息
    @Test
    public void test() {
        sender.send();
    }



}
